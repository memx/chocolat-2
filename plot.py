# -*- coding: utf-8 -*-
"""
Choccolat PLOTTER {um,ps,nC,J}
"""

# libraries
import matplotlib.style as style
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd

from scipy.optimize import curve_fit
from scipy.interpolate import griddata

import math
import ast
import time
 	
import os

#reset style for plots
style.use('default')
plt.rcParams['errorbar.capsize'] = 3
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = "22"
plt.rc('mathtext', fontset="cm")
plt.tick_params(axis = 'both', which = 'major', labelsize = 22)
plt.rcParams['figure.figsize'] = [8,4]
plt.rcParams['figure.dpi'] = 80
plt.rcParams['savefig.dpi'] = 600
#plt.axes().set_aspect(5.0)

# Constant Speed of Light
vc = 299.792458

# What to do?
plot = 1
showplots = 0

# Default 2D-contour plots are charging and hot electron temperature, auxiliarly define..
aux_x_axis = "metal"
aux_y_axis = "t_laser"
aux_z_cbar = "Q [nC]"
# .. with   metal   :: target material [CHoCoLaT]
#           d_targe :: target diameter [um]
#           e_targe :: target thickness [um]
#           N_iks   :: electron distribution energy bins [#]
#           Nb Th   :: maximum energy of electron distribution in terms of multiples of T_hot_e
#           dt      :: simulated time step [ps]
#           t_laser :: FWHM laser duration [ps]
#           r_laser :: FWHM laser spot radius [um]
# .. where evolving parameters are ploted with their 'final'/'maximum'/'sum' value
aux_shrink_evolution = 'final'
# .. and plots are seperated for different sets of non-evolving input parameters
aux_differ = ["metal","d_targe","e_targe","E_laser","w_laser"]

# Additional 2D plots where one/two y-axis is/are is plotted evolving with x-axis..
add_x_axis = "time [ps]"
add_xscale='linear'
add_x_axislim = None # array of two numbers or 'None'

add_y_axis = ("J tot [kA]","Q [nC]")
add_yscale='linear'
add_y_axislim_1 = None # array of two numbers or 'None'
add_y_axislim_2 = None # array of two numbers or 'None'

# .. with strings/tuples named as variables for auxiliary plots
# .. and plots are seperated for different sets of non-evolving input parameters
add_differ = ["metal","d_targe","e_targe","w_laser"]
# .. and legend will be added automatically to compare else differing sets, inside one and the same plot.

# Input Files
skiplines = 19
basename = 'output_scalar_'
data_cols = ["time [ps]","radius [µm]","lambdaD [µm]","Efr [keV]","Ere [keV]",
                            "Qt [nC]","pot th [keV]","pot E [keV]","J fr [kA]","J re [kA]","Q [nC]","E_cloud [J]"," E_heated [J]","E_ejected [J]","(E_c+E_h)/all"]
                            
derived_cols = ["J tot [kA]"]

# Take into account Simulations in specific parameter range
#  tuple ::  one value  : this precice value
#            two values : all values within closed interval
parameter_range = {}

parameter_range["metal"]   =  (2,16)
parameter_range["N_iks"]   =  (20000,) # Energy bins
parameter_range["dt"]      =  (0.001,)
parameter_range["Nb Th"]     =  (20,) # Max N*T_h in [eV]

parameter_range["t_laser"] =  (0.03,0.06)
parameter_range["E_laser"] =  (1.,8.)
parameter_range["C_abs"]   =  (1.,)
parameter_range["w_laser"] =  (0.800,)
parameter_range["d_laser"] =  (12.,)

parameter_range["d_targe"] =  (10000.,)
parameter_range["e_targe"] =  (1.,100.)

parameter_dtype = [('metal', "i8"),('N_iks', "i8"),('dt', "float"),
                   ('Nb Th', "i8"),('t_laser', "float"),('E_laser', "float"),
                   ('C_abs', "float"),('w_laser', "float"),('d_laser', "float"),
                   ('d_targe', "float"),('e_targe', "float")]

# Criteria for PLOTS

plotrange_tL = [0.2,0.7]
plotrange_rL = [10.,14.]

# If only plots are wished, the indication of evaluation points can be chosen
showevalpoints = False

# Get Going
def plotter7(x_base,z_toplot,path,x_label,z_label,label_title,showplots):
    fig = plt.figure()
    ax = plt.axes()
    
    fig.suptitle(label_title)
    
    p1 = ax.plot(x_base, z_toplot)

    ax.set_xlabel(x_label)
    ax.set_ylabel(z_label)
    
    plt.tight_layout()
    plt.savefig(path)
        
    if showplots == 1 : plt.show()
    plt.clf()

#PGS
print("Coucou - let's see some ChoCoLaT")
print(" ")
cwd = os.getcwd()
print(' WORK in '+cwd)
print(" ")
                    
if plot == 1 :

    print(' LOAD DATA')
    print(" ")
    
    # Load Files .. Unestethic but efficient..
    f = []
    for (dirpath, dirnames, filenames) in os.walk(cwd):
        f.extend(filenames)
        break
    # Empty Containers for DATA and for Bundles of Parameters to be studied seperately
    DATA = {}
    aux_differ_meta = []
    add_differ_meta = []
    # Import of the data into a dictionary
    for fi,filename in enumerate([string for string in f if basename in string]):
        # Meta Data
        interesting = True
        with open(cwd+'/'+filename,"r") as ifile:
            #print(' > '+filename)
            meta_data = np.zeros((1,),dtype = parameter_dtype)
            auxd_data = np.zeros((1,),dtype = [key for key in parameter_dtype if key[0] in aux_differ])
            addd_data = np.zeros((1,),dtype = [key for key in parameter_dtype if key[0] in add_differ])
            for line in ifile:
                if line[0] == '%':
                    #print('   READ :: '+line.strip('\n'))
                    line = line[2:].split('=')
                    name = line[0].strip()
                    if name in [key[0] for key in parameter_dtype]:
                        # Convert Value to correct type
                        value= ast.literal_eval(line[1].strip())
                        # Save for Meta..
                        meta_data[name] = value
                        # .. and for plots
                        if name in aux_differ:
                            auxd_data[name] = value
                        if name in add_differ:
                            addd_data[name] = value
                        # Feedback
                        #print('   LOAD :: '+str(name)+' => '+str(value))
                        if len(parameter_range[name]) == 1:
                            if parameter_range[name][0] != value:
                                interesting = False
                        elif len(parameter_range[name]) == 2:
                            if not (parameter_range[name][0] <= value and value <= parameter_range[name][1]):
                                interesting = False
                        else:
                            print("   FAULTY 'Input Range'")
                            interesting = False
                            break
                    #else:
                        #print("   UNKNOWN 'dtype' FOR "+name)
            #print(' ') 
        # Detailed Evolution Data if Meta is in Ranges of interest            
        if interesting:
            # Load Details
            ifile = pd.read_csv(cwd+'/'+filename, sep=" ",skipinitialspace=True, 
                                skip_blank_lines=True, header=0, skiprows=skiplines, # comment="%",
                                mangle_dupe_cols = True)
            ifile.columns = data_cols

            # Data Structure
            if meta_data["metal"][0] not in DATA:
                DATA[meta_data["metal"][0]] = {}
            if meta_data["d_targe"][0] not in DATA[meta_data["metal"][0]]:
                DATA[meta_data["metal"][0]][meta_data["d_targe"][0]] = {}
            if meta_data["e_targe"][0] not in DATA[meta_data["metal"][0]][meta_data["d_targe"][0]]:
                DATA[meta_data["metal"][0]][meta_data["d_targe"][0]][meta_data["e_targe"][0]] = []
            # Paste Data
            DATA[meta_data["metal"][0]][meta_data["d_targe"][0]][meta_data["e_targe"][0]].append([meta_data,ifile])
            # Parse Meta to Bundle
            aux_differ_meta.append(auxd_data)
            add_differ_meta.append(addd_data)
    # Remember equal sets and delete doubles
    aux_differ_meta = np.unique(aux_differ_meta, axis=0)
    add_differ_meta = np.unique(add_differ_meta, axis=0)
    # Take TIMESTEMP
    ts = str(int(time.time()))
    # Go Through Target Parameters
    for it_metal in DATA.keys():
        for it_d_targe in DATA[it_metal].keys():
            for it_e_targe in DATA[it_metal][it_d_targe].keys():
    
                # Container for Final Data with no Regard to Detailed Evolution
                N_iks_container = []
                dt_container = []
                Nb_Th_container = []
                t_laser_container = []
                E_laser_container = []
                C_abs_container = []
                w_laser_container = []
                d_laser_container = []
                Q_container = []
    
                # Parse Data
                for idd,dataset in enumerate(DATA[it_metal][it_d_targe][it_e_targe]):
                    N_iks_container.append(dataset[0]["N_iks"][0])
                    dt_container.append(dataset[0]["dt"][0])
                    Nb_Th_container.append(dataset[0]["Nb Th"][0])
                    t_laser_container.append(dataset[0]["t_laser"][0])
                    E_laser_container.append(dataset[0]["E_laser"][0])
                    C_abs_container.append(dataset[0]["C_abs"][0])
                    w_laser_container.append(dataset[0]["w_laser"][0])
                    d_laser_container.append(dataset[0]["d_laser"][0])
                    Q_container.append(dataset[1]["Q [nC]"].iloc[-1])
                    
                    # ... plots relevant to single simulations
                    savename = ts
                    for name in [item[0] for item in parameter_dtype]:
                        savename = savename+'_'+name.replace(' ','')+'_'+str(dataset[0][name][0])
                    
                    # Container for renorm of integral of Q
                    dQ = dataset[1]["Q [nC]"]
                    totalQ = dataset[1]["Q [nC]"].iloc[-1]
                    
                    # Container for variable timestep
                    dtdt = dataset[1]["time [ps]"]
                    
                    # Container for evolution of Q
                    container = []
                    expanddQ = []
                    expanddtdt = []
                    xNt = 0 # inject extrapolated charging steps
                    for key in range(len(dQ.index)*(xNt+1)):
                        keymod = float(key) % float(xNt+1)

                        # LOOK FOR BOUNDARY
                        chargekey = math.floor(float(key)/float(xNt+1))
                        if chargekey == len(dQ.index) - 1:
                            break
                        
                        # linear extrapolation of discrete charge steps
                        if key == 0:
                            container.append(dQ[chargekey])
                        else:
                            if chargekey == 0:
                                container.append((dQ[chargekey]-0.)/float(xNt+1))
                            else:
                                container.append((dQ[chargekey]-dQ[chargekey-1])/float(xNt+1))
                        
                        # linear extrapolation of timesteps
                        expanddtdt.append(dtdt[chargekey] + (dtdt[chargekey+1]-dtdt[chargekey]) \
                                                    * (keymod)/float(xNt+1))
   
                        #expanddQ.append(dQ[chargekey]) ! TODO A
                        expanddQ.append(dQ[chargekey] + (dQ[chargekey+1]-dQ[chargekey]) \
                                                    * (keymod)/float(xNt+1))
                    
                    dQdt = np.array(container)
                    dQ   = np.array(expanddQ)
                    dtdt   = np.array(expanddtdt)
                    
                    timesteps = len(dQdt)
                    
                    # Plot RAW
                    plotter7(dtdt,dQdt,'discharge_'+savename.replace('.','p')+'.png',\
                                    r't / [ps]',r'$\Delta t \cdot \partial_t Q$ / [nC]','Q = '+str(totalQ)+'nC',showplots)
                                    
                    # Add details (paandas add id-wise)
                    for add_detail in derived_cols:
                        time_steps = dataset[1]["time [ps]"].diff()
                        time_steps[0] = dataset[1]["time [ps]"][0]
                        if add_detail == "J tot [kA]":
                            j_tot = dataset[1]["J fr [kA]"] + dataset[1]["J re [kA]"]
                            add_column = j_tot
                        DATA[it_metal][it_d_targe][it_e_targe][idd][1][add_detail] = np.array(add_column)
                    
                # .. prepare intercomparison 
                # Evaluate Data Energy Resolution
                r_laser = np.array(d_laser_container)/2./math.sqrt(math.log(2.)) #Gausz Spot
                t_laser = np.array(t_laser_container)*math.sqrt(math.pi)/ (2.*math.sqrt(math.log(2.))) #Gausz Pulse
                E_depot = np.array(E_laser_container)*np.array(C_abs_container)
                P_laser = E_depot/t_laser
                I_laser = P_laser  / ( math.pi* r_laser**2)
                
                a0 = 0.853*np.array(w_laser_container)*10.*np.sqrt(I_laser)
                
                T_hot_e = []
                
                for a in a0:
                    if a > 0.03 :
                      T_hot_e.append(max(0.47*a**(2./3.), math.sqrt(1.+a**2)-1.))
                    else :
                      T_hot_e.append(3.*a**(4./3.))

                E_res = np.array(Nb_Th_container) * np.array(T_hot_e) / np.array(N_iks_container)
    
                # GLOBAL PLOTS WITH FWHM r and t
                
                savename = ts+'_'+str(it_metal)+'_'+str(it_d_targe)+'_'+str(it_e_targe)
                
                x_base = np.array(t_laser_container) * (np.array(d_laser_container)/2.)**2
                y_base = E_depot * np.array(w_laser_container)**2
                Q_base = np.array(Q_container)
                T_base = np.array(T_hot_e)
                
                if len(list(set(x_base))) > 1 and  len(list(set(y_base))) > 1:
                    # Do the Global Contour 2D :: Charges
                    fig = plt.figure()
                    ax = plt.axes()
                    p1 = ax.tricontourf(x_base, y_base, Q_base, cmap=cm.viridis)

                    ax.set_xlabel(r'$\tau_L r_L^2$ / [ps $\mu$m$^2$]')
                    ax.set_ylabel(r'$c_\mathrm{abs} E_L \lambda_L$ / [J $\mu$m$^2$]')

                    fig.colorbar(p1, shrink=1., aspect=5, label=r'$Q_0^C$ / [nC]')
                    
                    plt.tight_layout()
                    plt.savefig('plotter_'+savename.replace('.','p')+'_Q(E_L-lam_L-tau_L-r_L).png')
                        
                    if showplots == 1 : plt.show()
                    plt.clf()
                    
                    # Do the Global Contour 2D :: T_hot
                    fig = plt.figure()
                    ax = plt.axes()
                    
                    p1 = ax.tricontourf(x_base, y_base, Q_base, cmap=cm.viridis)

                    ax.set_xlabel(r'$\tau_L r_L^2$ / [ps $\mu$m$^2$]')
                    ax.set_ylabel(r'$c_\mathrm{abs} E_L \lambda_L$ / [J $\mu$m$^2$]')

                    fig.colorbar(p1, shrink=1., aspect=5, label=r'$Q_0^C$ / [nC]')
                    
                    plt.tight_layout()
                    plt.savefig('plotter_'+savename.replace('.','p')+'_T_h(E_L-lam_L-tau_L-r_L).png')
                        
                    if showplots == 1 : plt.show()
                    plt.clf()
                
                # Do r-tau-plots
                savename = ''
                avail_w_laser = list(set(w_laser_container))
                avail_E_depot = list(set(E_depot))
                for it_w in avail_w_laser:
                    for it_E in avail_E_depot:
                        savename = ts+'_'+str(it_metal)+'_'+str(it_d_targe)+'_'+str(it_e_targe)+'_'+str(it_w)+'_'+str(it_E)
                        x_t_laser = []
                        y_r_laser = []
                        z_Q = []
                        z_T = []
                        # Select Data
                        for t_l,r_l,w_l,E_d,Qextr,Thote in zip(t_laser_container,np.array(d_laser_container)/2.,w_laser_container,E_depot,Q_container,T_hot_e):
                            if it_w == w_l and it_E == E_d:
                                x_t_laser.append(t_l)
                                y_r_laser.append(r_l)
                                z_Q.append(Qextr)
                                z_T.append(Thote)
                        # Plot Data
                        if len(x_t_laser) > 1:
                            # Common Canvas
                            minx = min(x_t_laser)
                            maxx = max(x_t_laser)
                            miny = min(y_r_laser)
                            maxy = max(y_r_laser)
                            
                            xi = np.linspace(minx,maxx,200)
                            yi = np.linspace(miny,maxy,40)
                            
                            # Charge Plot
                            try:
                                fig = plt.figure()
                                ax = plt.axes()
                                
                                zi = griddata((x_t_laser, y_r_laser), np.array(z_Q), (xi[None,:], yi[:,None]), method='linear')
                                
                                p2 = ax.contour(xi, yi, zi, linewidths=0.5,colors='k')
                                
                                cbar_min = np.min(z_Q)
                                cbar_max = np.max(z_Q)
                                levelsc = 35
                                cbarlabels = np.linspace(np.floor(cbar_min), np.ceil(cbar_max), num=levelsc, endpoint=True)
                                
                                p3 = ax.contourf(xi, yi, zi, levelsc, vmin=cbar_min, vmax=cbar_max, cmap='viridis') #,norm = LogNorm())
                                
                                if showevalpoints:
                                    ax.plot(x_t_laser, y_r_laser, 'ko', ms=5)
                                
                                ax.set_xlabel(r'$\tau_L$ / [ps]')
                                ax.set_ylabel(r'$r_L$ / [$\mu$m]')

                                cobar = fig.colorbar(p3, shrink=1.0, aspect=5, label=r'$Q_0^C$ / [nC]')
                                
                                # TODO special markers
                                #ax.axvline(x = ftL, linestyle='--', color='k')
                                #ax.axhline(y = frL, linestyle='--', color='k')
                                
                                plt.title(r'$E_L $ = '+str(it_E)+r'J')
                                
                                # TODO Crop Canvas
                                #tickstep = (max(plotrange_tL)-min(plotrange_tL))/3.
                                #ax.set_xticks([min(plotrange_tL) + w*tickstep for w in range(4)])
                                
                                #ax.set_ylim(plotrange_rL)
                                #ax.set_xlim(plotrange_tL)
                                
                                plt.tight_layout()
                                
                                plt.savefig('plotter_'+savename.replace('.','p')+'_Q(tau_L-r_L).png')

                                if showplots == 1 : plt.show()
                                plt.clf()
                            
                            except:
                                print("   PLOT ERROR 'Charge Plot'")
                            
                            # T_hot_e Plot
                            try:
                                fig = plt.figure()
                                ax = plt.axes()
                                
                                zi = griddata((x_t_laser, y_r_laser), np.array(z_T), (xi[None,:], yi[:,None]), method='linear')
                                
                                p4 = ax.contour(xi, yi, zi, linewidths=0.5,colors='k')
                                
                                cbar_min = np.min(z_Q)
                                cbar_max = np.max(z_Q)
                                levelsc = 35
                                cbarlabels = np.linspace(np.floor(cbar_min), np.ceil(cbar_max), num=levelsc, endpoint=True)
                                
                                p5 = ax.contourf(xi, yi, zi, levelsc, vmin=cbar_min, vmax=cbar_max, cmap='viridis') #,norm = LogNorm())
                                
                                if showevalpoints:
                                    ax.plot(x_t_laser, y_r_laser, 'ko', ms=5)
                                
                                ax.set_xlabel(r'$\tau_L$ / [ps]')
                                ax.set_ylabel(r'$r_L$ / [$\mu$m]')

                                cobar = fig.colorbar(p5, shrink=1.0, aspect=5, label=r'$T_h^e$ / [MeV]')
                                
                                # TODO special markers
                                #ax.axvline(x = ftL, linestyle='--', color='k')
                                #ax.axhline(y = frL, linestyle='--', color='k')
                                
                                plt.title(r'$E_L $ = '+str(it_E)+r'J')
                                
                                # TODO Crop Canvas
                                #tickstep = (max(plotrange_tL)-min(plotrange_tL))/3.
                                #ax.set_xticks([min(plotrange_tL) + w*tickstep for w in range(4)])
                                
                                #ax.set_ylim(plotrange_rL)
                                #ax.set_xlim(plotrange_tL)
                                
                                plt.tight_layout()
                                
                                plt.savefig('plotter_'+savename.replace('.','p')+'_The(tau_L-r_L).png')

                                if showplots == 1 : plt.show()
                                plt.clf()
                            
                            except:
                                print("   PLOT ERROR 'T_hot_e Plot'")
    
    # Prepare for all Plots
    meta_parameters = list(set(parameter_range.keys()))
    data_evolutions = data_cols + derived_cols
    
    # Go Through USER DEMAND :: 2D-CONTOUR PLOTS (AUX)
    #  aux_differ_meta ::   Go through subset of data with similar characteristics 
    #                       to be seperated in different plots
    #aux_wrapper = []
    for aux_meta in aux_differ_meta:
        x_container = []
        y_container = []
        z_container = []
        # SET UP ARRAYS
        for it_metal in DATA.keys():
            for it_d_targe in DATA[it_metal].keys():
                for it_e_targe in DATA[it_metal][it_d_targe].keys():
                    for dataset in DATA[it_metal][it_d_targe][it_e_targe]:
                        # CHECK FAMILY : ARE WE IN THE SAME AUX_DIFFER SET?
                        familymember = True
                        for ad in aux_differ:
                            if aux_meta[ad][0] != dataset[0][ad][0]:
                                familymember = False
                        if familymember:
                            # CHECK X COORDINATE
                            if aux_x_axis in meta_parameters:
                                x_container.append(dataset[0][aux_x_axis][0])
                            elif aux_x_axis in data_evolutions:
                                if aux_shrink_evolution == 'final':
                                    x_container.append(dataset[1][aux_x_axis].iloc[-1])
                                elif aux_shrink_evolution == 'maximum':
                                    x_container.append(dataset[1][aux_x_axis].max())
                                elif aux_shrink_evolution == 'sum':
                                    x_container.append(dataset[1][aux_x_axis].sum())
                                else:
                                    print("   UNKNOWN 'aux_shrink_evolution'")
                            else:
                                print("   UNKNOWN 'aux_x_axis'")
                            # CHECK Y COORDINATE
                            if aux_y_axis in meta_parameters:
                                y_container.append(dataset[0][aux_y_axis][0])
                            elif aux_y_axis in data_evolutions:
                                if aux_shrink_evolution == 'final':
                                    y_container.append(dataset[1][aux_y_axis].iloc[-1])
                                elif aux_shrink_evolution == 'maximum':
                                    y_container.append(dataset[1][aux_y_axis].max())
                                elif aux_shrink_evolution == 'sum':
                                    y_container.append(dataset[1][aux_y_axis].sum())
                                else:
                                    print("   UNKNOWN 'aux_shrink_evolution'")
                            else:
                                print("   UNKNOWN 'aux_y_axis'")
                            # CHECK Z COORDINATE
                            if aux_z_cbar in meta_parameters:
                                x_container.append(dataset[0][aux_z_cbar][0])
                            elif aux_z_cbar in data_evolutions:
                                if aux_shrink_evolution == 'final':
                                    z_container.append(dataset[1][aux_z_cbar].iloc[-1])
                                elif aux_shrink_evolution == 'maximum':
                                    z_container.append(dataset[1][aux_z_cbar].max())
                                elif aux_shrink_evolution == 'sum':
                                    z_container.append(dataset[1][aux_z_cbar].sum())
                                else:
                                    print("   UNKNOWN 'aux_shrink_evolution'")
                            else:
                                print("   UNKNOWN 'aux_z_cbar'")
        # Parse Data
        #aux_wrapper.append([x_container,y_container,z_container])  
        # Plot Data
        savename = ts
        for name in aux_differ:
            savename = savename+'_'+name.replace(' ','')+'_'+str(aux_meta[name][0])
        minx = min(x_container)
        maxx = max(x_container)
        miny = min(y_container)
        maxy = max(y_container)
        
        xi = np.linspace(minx,maxx,200)
        yi = np.linspace(miny,maxy,40)
        
        try: 
        
            constructor_name = 'plotter'+savename.replace('.','p')+'_'+aux_z_cbar.replace(' ','')+"("+aux_x_axis.replace(' ','')+'-'+aux_y_axis.replace(' ','')+')'+'.png'
            
            print("   TRY "+constructor_name)
            
            fig = plt.figure()
            ax = plt.axes()
            
            zi = griddata((x_container, y_container), np.array(z_container), (xi[None,:], yi[:,None]), method='linear')
            
            pA = ax.contour(xi, yi, zi, linewidths=0.5,colors='k')
            
            cbar_min = np.min(z_container)
            cbar_max = np.max(z_container)
            levelsc = 35
            cbarlabels = np.linspace(np.floor(cbar_min), np.ceil(cbar_max), num=levelsc, endpoint=True)
            
            pB = ax.contourf(xi, yi, zi, levelsc, vmin=cbar_min, vmax=cbar_max, cmap='viridis') #,norm = LogNorm())
            
            if showevalpoints:
                ax.plot(x_container, y_container, 'ko', ms=5)
            
            ax.set_xlabel(aux_x_axis)
            ax.set_ylabel(aux_y_axis)

            cobar = fig.colorbar(pB, shrink=1.0, aspect=5, label=aux_z_cbar)
            
            # TODO special markers
            #ax.axvline(x = ftL, linestyle='--', color='k')
            #ax.axhline(y = frL, linestyle='--', color='k')
            
            # TODO Title
            # plt.title(r'META = '+str(it_E)+r'J')
            
            # TODO Crop Canvas
            #tickstep = (max(plotrange_tL)-min(plotrange_tL))/3.
            #ax.set_xticks([min(plotrange_tL) + w*tickstep for w in range(4)])
            
            #ax.set_ylim(plotrange_rL)
            #ax.set_xlim(plotrange_tL)
            
            plt.tight_layout()
            
            plt.savefig(constructor_name)

            if showplots == 1 : plt.show()
            plt.clf()
            
        except:
            print("   PLOT ERROR 'AUX Plot'")

    # Go Through USER DEMAND :: ADD 2D PLOTS (WITH LEGEND)
    add_legend = [item for item in meta_parameters if item not in add_differ and item not in add_y_axis and item != add_x_axis]
    #  add_differ_meta ::   Go through subset of data with similar characteristics 
    #                       to be seperated in different plots
    #add_wrapper = []
    for add_meta in add_differ_meta:
        constructor_name = 'plotter'+savename.replace('.','p')+'_'+str(add_y_axis).replace(' ','').replace(',','-')+"("+add_x_axis.replace(' ','')+')'+'.png'
        
        print("   TRY "+constructor_name)
        
        # Plot Layout
        fig, ax1 = plt.subplots()
        cmap = cm.viridis 

        ax1.set_xlabel(add_x_axis)
        ax1.set_xscale(add_xscale)
        ax1.set_ylabel(add_y_axis[0])
        ax1.set_yscale(add_yscale)
        
        ax1.tick_params(axis='y')

        ax1.grid(b=True, which='major', axis='both',linestyle='-',linewidth=2)
        plt.minorticks_on()
        ax1.grid(b=True, which='minor', axis='both',linestyle='--',linewidth=1)
        
        if len(add_y_axis) > 1:
            ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
            ax2.set_ylabel(add_y_axis[1])  # we already handled the x-label with ax1
            ax2.set_yscale(add_yscale)
            ax2.tick_params(axis='y')
        if len(add_y_axis) > 2:
            print("   IGNORE 'add_y_axis[2:]'")
        # LEGEND TITLE
        ltitle = ''
        for ad in add_legend:
            ltitle = ltitle+ad.replace(' ','')+'; '
        # SET UP ARRAYS
        plot_counter = 0
        for it_metal in DATA.keys():
            for it_d_targe in DATA[it_metal].keys():
                for it_e_targe in DATA[it_metal][it_d_targe].keys():
                    for dataset in DATA[it_metal][it_d_targe][it_e_targe]:
                        # CHECK FAMILY : ARE WE IN THE SAME ADD_DIFFER SET?
                        familymember = True
                        for ad in add_differ:
                            if add_meta[ad][0] != dataset[0][ad][0]:
                                familymember = False
                        if familymember:
                            # Legend Entry
                            legendname = ''
                            for ad in add_legend:
                                legendname = legendname+str(dataset[0][ad][0])+'; '
                            
                            # Assemble DATA
                            x_container = []
                            y_container = []
                            if len(add_y_axis) > 1:
                                y2_container = []
                            # CHECK X COORDINATE
                            if add_x_axis in meta_parameters:
                                x_container.append(dataset[0][add_x_axis][0])
                            elif add_x_axis in data_evolutions:
                                x_container = list(dataset[1][add_x_axis])
                            else:
                                print("   UNKNOWN 'add_x_axis'")
                            # CHECK Y COORDINATE
                            if add_y_axis[0] in meta_parameters:
                                y_container.append(dataset[0][add_y_axis[0]][0])
                            elif add_y_axis[0] in data_evolutions:
                                y_container = list(dataset[1][add_y_axis[0]])
                            else:
                                print("   UNKNOWN 'add_y_axis[0]'")
                            # CHECK 2nd Y COORDINATE
                            if len(add_y_axis) > 1:
                                if add_y_axis[1] in meta_parameters:
                                    y2_container.append(dataset[0][add_y_axis[1]][0])
                                elif add_y_axis[1] in data_evolutions:
                                    y2_container = list(dataset[1][add_y_axis[1]])
                                else:
                                    print("   UNKNOWN 'add_y_axis[1]'")
                            # Expand meta_parameters to length of evolution
                            if len(x_container) == 1 and len(y_container) == 1:
                                print("   NO EVOLUTION OF "+add_y_axis[0]+"("+add_x_axis+")")
                            elif len(x_container) == 1 and len(y_container) > 1:
                                x_val = x_container[0]
                                x_container = [x_val for item in y_container]
                            elif len(x_container) > 1 and len(y_container) == 1:
                                y_val = y_container[0]
                                y_container = [y_val for item in x_container]
                            if len(add_y_axis) > 1:
                                if len(x_container) == 1 and len(y2_container) == 1:
                                    print("   NO EVOLUTION OF "+add_y_axis[1]+"("+add_x_axis+")")
                                elif len(x_container) == 1 and len(y2_container) > 1:
                                    x_val = x_container[0]
                                    x_container = [x_val for item in y2_container]
                                elif len(x_container) > 1 and len(y2_container) == 1:
                                    y_val = y2_container[0]
                                    y2_container = [y_val for item in x_container]
                            # CHECK DATA AND PLOT
                            if len(x_container) == len(y_container):
                                ax1.plot(x_container,y_container, color = 'k', ls = '--')
                                ax1.scatter(x_container,y_container, cmap = cmap, label = legendname)
                                plot_counter += 1
                            else:
                                print("   FAULTY 'len(x_container) == len(y_container)'")
                            if len(add_y_axis) > 1:
                                if len(x_container) == len(y2_container):
                                    ax2.plot(x_container,y2_container, color = 'k', ls = '-.')
                                    ax2.scatter(x_container,y2_container, cmap = cmap, label = legendname)
                                    plot_counter += 1
                                else:
                                    print("   FAULTY 'len(x_container) == len(y2_container)'")
        # Parse Data
        #add_wrapper.append([x_container,y_container,z_container])  
    
        # Plot Data
        savename = ts
        for name in add_differ:
            savename = savename+'_'+name.replace(' ','')+'_'+str(add_meta[name][0])
        
        if showevalpoints:
            ax1.plot(x_container, y_container, 'ko', ms=5)
            if len(add_y_axis) > 1:
                ax2.plot(x_container, y2_container, 'ko', ms=5)
        
        # Finalize Plot
        if add_x_axislim is not None:
            ax1.set_xlim(add_x_axislim)
        if add_y_axislim_1 is not None:
            ax1.set_ylim(add_y_axislim_1)
        if add_y_axislim_2 is not None and len(add_y_axis) > 1:
            ax2.set_ylim(add_y_axislim_2)
        
        col_count = math.ceil((plot_counter / 2) / 10)
        legenda = ax1.legend(bbox_to_anchor=(1.15, 1), loc='upper left', ncol=col_count, title=ltitle, fontsize='small', )
        fig.set_size_inches(12.0, 6.0)
        plt.gcf().canvas.draw()
        invFigure = plt.gcf().transFigure.inverted()
        lgd_pos = legenda.get_window_extent()
        lgd_coord = invFigure.transform(lgd_pos)
        lgd_xmax = lgd_coord[1, 0]
        ax_pos = plt.gca().get_window_extent()
        ax_coord = invFigure.transform(ax_pos)
        ax_xmax = ax_coord[1, 0]
        shift = 1 - (lgd_xmax - ax_xmax)
        plt.gcf().tight_layout(rect=(0, 0, shift, 1))
        
        #fig.tight_layout()
        plt.savefig(constructor_name, bbox_extra_artists=[legenda], bbox_inches='tight')

        if showplots == 1 : plt.show()
        plt.clf()

print(" EXITCUTE")
               
