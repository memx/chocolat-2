! CHOCOLAT CHARGE DENSITY SIMULATION BY MEmx 2021
! @author:      Michael Ehret
! @initial:     12/2021
! @version:     20220110
! @owner1:      Michael Ehret
! @licensee:    Developers's Version
! #############################################################
! #############################################################
! CONFIDENTIALITY NOTICE: This document, including any attach-
! ments, may contain information that is protected by French or
! European privacy acts. This code is intended solely for the
! addressee(s). If you are not the intended recipient, you are
! hereby notified that you are not authorized to read, print,
! retain, copy, disclose, distribute, or use this document, any
! part of it, or any attachments. If you have received this
! code in error, please immediately notify the sender by e-mail
! or return e-mail and delete this document and any attachments
! from your system without reading or saving in any manner.
! Please destroy this document in any form if you found it and
! after the expiration date if you are the licensee.
! #############################################################
! #############################################################
!
! This program simulates the growth of the central part of the
! charge density resulting from ChoCoLaT discharge.
!
! All SI units, with nC-charge, a mm-space and ps-timescale.
!
! Clean up directory with the following command before compile
! rm -f ch *.o *.mod *.dep
!
! Compile with command
! gfortran -O3 -fopenmp "chchd.f90" -o chchd
! Run via Terminal
! ./chd path/to/chocolat/outut_sclar.txt
! Append input files if needed seperated by a Blank
!
! #############################################################
! #############################################################
! HEAD: PREPARE META MODULES
! #############################################################
MODULE methods_system
    ! VARIABLES
    IMPLICIT none
    !> check if exit is already initiated elsewhere
    LOGICAL :: is_exiting = .false.
    !> define real kind
    INTEGER, PARAMETER :: q = selected_real_kind(30,291)  !! decimals, exponent range, e.g. 30,291
CONTAINS
    ! SYSTEM FUNCTIONS
    ! Function to check availability of selected real kind
    SUBROUTINE ask_selectedrealkind()
        IF (q .le. 0) THEN
            PRINT*, "> RETURN FAULTY REAL KIND ",q
            CALL exit_chchd("Kind unavailable ")
        ELSE
            PRINT*, "> SELECTED REAL KIND ",q
        END IF
    END SUBROUTINE ask_selectedrealkind
    ! Function to give timestam
    FUNCTION ask_timestamp()
        REAL(kind=q) :: ask_timestamp
        ask_timestamp = time8()
    END FUNCTION ask_timestamp
    ! Subroutine to exit program gracefully, closing all open files
    SUBROUTINE exit_chchd(err_msg)
        ! Optional Error message with variable length
        CHARACTER(len=*), INTENT(IN), OPTIONAL :: err_msg

        ! Internal Variables
        INTEGER :: i, stat
        LOGICAL :: is_file_open

        ! Only output exit once
        IF (is_exiting) THEN
            RETURN
        ELSE
            is_exiting = .true.
        END IF

        ! Check all units
        ! TO DO: Check if there is a better way to collect all open files
        ! Maybe an array which populates when files opened
        ! Depopulates when they are closed
        DO i=1, 1000
            ! Check if a file is connected to given unit
            INQUIRE(UNIT=i, OPENED=is_file_open)

            ! File is connected to a unit..
            IF (is_file_open) THEN
                ! Close the file
                CLOSE(UNIT=i, IOSTAT=stat)
            END IF
        END DO

        ! Stop program with error message (if provided)        
        IF (present(err_msg)) THEN
            !$OMP CRITICAL
            PRINT*, trim(err_msg)
            !$OMP END CRITICAL
            STOP
        ELSE
            STOP "Error! Exiting CHCHD."
        END IF
    END SUBROUTINE exit_chchd
    ! CREATE NEW DIRECTORY
    SUBROUTINE create_outputdirectory(do_path)
        ! optional input, else output in working directory output/
        CHARACTER(len=*), INTENT(IN), OPTIONAL :: do_path
        ! Internal VARIABLES
        CHARACTER(len=1024) :: path = 'output'  !! default output path
        INTEGER :: exit_stat                    !! container for file stats
        ! MAIN
        ! overwrite default if applicable
        IF (present(do_path)) write(path,'(a)') do_path
        ! check if directory exist in which we want to store results
        PRINT*, "> WORK ON OUTPUT DIRECTORY "
        PRINT*, "  "//trim(path) 
        CALL SYSTEM("dir "//trim(path)//" > listing.dat",exit_stat)
        ! create or keep feet still
        IF (exit_stat /= 0) THEN
            PRINT*, "  * CREATE "
            PRINT*, "    "//trim(path)
            CALL SYSTEM('mkdir '//trim(path))
        ELSE
            PRINT*, "  * DIRECTORY EXISTS"
        ENDIF
        ! clean up
        CALL SYSTEM("del  listing.dat",exit_stat) !! windows
        CALL SYSTEM("rm  listing.dat",exit_stat)  !! posix
        ! END MAIN
    END SUBROUTINE create_outputdirectory
    
END MODULE methods_system

MODULE methods_caretaker
    ! DEPENDENCIES
    USE methods_system
    ! VARIABLES
    IMPLICIT none
    !> simulation id
    INTEGER, PUBLIC :: wsid
CONTAINS
    ! SIMULATION WIDE HOUSEKEEPING
    ! define simulation ID
    FUNCTION set_simulationid()
        IMPLICIT none
        INTEGER :: set_simulationid
        set_simulationid = ask_timestamp()
   END FUNCTION set_simulationid
END MODULE methods_caretaker
! #############################################################
! #############################################################
! HEAD: PREPARE DATA BROKER MODULES
! #############################################################
MODULE types
    ! DEPENDENCIES
    USE methods_system, ONLY: q
    ! VARIABLES
    IMPLICIT none
    ! PUBLIC TYPES
    ! for chocolat meta: input variables
    TYPE :: header
        ! meta data
        INTEGER :: metal     !! (1:carbone 2:aluminum 3:cuivre 4:tungstene 5:tantale 6:plomb)
        INTEGER :: N_iks     !! (number of point for the distribution function discretisation in mec**2)
        REAL(kind=q)  :: dt        !! (time step for initial value solver, in ps)
        INTEGER :: Nb_Th     !! (energy maximum of the distribution function as Nb_th * Th (eq:B1) )
        INTEGER :: diag_s    !! (1 save every diag_s iterations for the scalar diagnostic. 0 is setting the diagnostic off )
        INTEGER :: diag_f    !! (1 save every diag_h iterations for the distribution function diagnostic. 0 is setting the diagnostic off )
        REAL(kind=q)  :: t_laser   !! (duration of the laser pulse, in ps)
        REAL(kind=q)  :: E_laser   !! (energy of the laser pulse, in J)                    
        REAL(kind=q)  :: C_abs     !! (absorbtion coefficent of the laser pulse, from 0 to 1)                    
        REAL(kind=q)  :: w_laser   !! (wavelength of the laser pulse, in um)                  
        REAL(kind=q)  :: d_laser   !! (diameter of the laser spot, in um)                    
        REAL(kind=q)  :: d_targe   !! (diameter of the target, in um)   
        REAL(kind=q)  :: e_targe   !! (thickness of the target, in um)
        REAL(kind=q)  :: phi_E_plugged, T_hot_e, N_tot, t_life
        
    END TYPE header
    
    ! for chocolat scalar data: main data columns
    TYPE :: scdata
        ! meta data
        REAL(kind=q), ALLOCATABLE :: time(:)     !! simulation time in s
        REAL(kind=q), ALLOCATABLE :: radius(:)   !! cloud radius average (limited by target radius) in um
        REAL(kind=q), ALLOCATABLE :: lambdaD(:)  !! hot electron Debyelenght in um
        REAL(kind=q), ALLOCATABLE :: Efr(:)      !! hot electron temperature (eq 11) in keV
        REAL(kind=q), ALLOCATABLE :: Ere(:)      !! in keV
        REAL(kind=q), ALLOCATABLE :: N(:)        !! number of hot electron (eq 10a) in nC
        REAL(kind=q), ALLOCATABLE :: pot_Th(:)   !! Thermal potential (eq 17 or 19) in keV
        REAL(kind=q), ALLOCATABLE :: pot_E(:)    !! Electrostatic potential (eq 22) in keV
        REAL(kind=q), ALLOCATABLE :: J_fr(:)     !! front side current ejected in kA
        REAL(kind=q), ALLOCATABLE :: J_re(:)     !! rear side current ejected in kA
        REAL(kind=q), ALLOCATABLE :: Q(:)        !! total charge ejected from the target in nC
        
    END TYPE scdata
    
CONTAINS
    ! ALLOCATION MANAGEMENT
    ! for main data frame
    SUBROUTINE scdata_allocate(scdata_var,i_int)
        ! VARIABLES
        TYPE(scdata), INTENT(INOUT) :: scdata_var
        INTEGER,      INTENT(IN)    :: i_int
        ! ALLOCATION
        IF (allocated(scdata_var%time)) DEALLOCATE(scdata_var%time)
        IF (allocated(scdata_var%radius)) DEALLOCATE(scdata_var%radius)
        IF (allocated(scdata_var%lambdaD)) DEALLOCATE(scdata_var%lambdaD)
        IF (allocated(scdata_var%Efr)) DEALLOCATE(scdata_var%Efr)
        IF (allocated(scdata_var%Ere)) DEALLOCATE(scdata_var%Ere)
        IF (allocated(scdata_var%N)) DEALLOCATE(scdata_var%N)
        IF (allocated(scdata_var%pot_Th)) DEALLOCATE(scdata_var%pot_Th)
        IF (allocated(scdata_var%pot_E)) DEALLOCATE(scdata_var%pot_E)
        IF (allocated(scdata_var%J_fr)) DEALLOCATE(scdata_var%J_fr)
        IF (allocated(scdata_var%J_re)) DEALLOCATE(scdata_var%J_re)
        IF (allocated(scdata_var%Q)) DEALLOCATE(scdata_var%Q)
        
        ALLOCATE(scdata_var%time(i_int))
        ALLOCATE(scdata_var%radius(i_int))
        ALLOCATE(scdata_var%lambdaD(i_int))
        ALLOCATE(scdata_var%Efr(i_int))
        ALLOCATE(scdata_var%Ere(i_int))
        ALLOCATE(scdata_var%N(i_int))
        ALLOCATE(scdata_var%pot_Th(i_int))
        ALLOCATE(scdata_var%pot_E(i_int))
        ALLOCATE(scdata_var%J_fr(i_int))
        ALLOCATE(scdata_var%J_re(i_int))
        ALLOCATE(scdata_var%Q(i_int))
        
    END SUBROUTINE scdata_allocate

END MODULE types

MODULE methods_io
    ! VARIABLES
    IMPLICIT none
    !> input filename (array)
    CHARACTER(len=:), ALLOCATABLE :: file_name_in(:)
    !> relative output path
    CHARACTER(len=9) :: set_path = 'output/'
    
CONTAINS
    ! INPUT DATA MANAGEMENT
    ! Read comand line
    SUBROUTINE parse_commandline(inputfile)
        !! Parse the command line arguments
        !> The name of the inputfile to read
        CHARACTER(len=:), ALLOCATABLE, INTENT(OUT) :: inputfile(:)

        INTEGER :: arg_count                 ! Number of command line arguments
        INTEGER :: length=0                  ! Length of given argument
        INTEGER :: largelength               ! Length of last argument
        CHARACTER(len=:), ALLOCATABLE :: arg ! The text of a given argument
        
        INTEGER :: i                         ! counting variable

        arg_count = COMMAND_ARGUMENT_COUNT()

        IF (arg_count == 0) THEN
            IF ( allocated(inputfile) ) deallocate(inputfile)
            ! DEBUGGING
            !allocate(character(len=121) :: inputfile(1))
            !inputfile(1) = '\\srvdocumentos\03Cientifico\Usuarios\mehret\Projects\' // &
            !             & 'HRR_helicoil\simulations\chocolat\1639665984\output_scalar_0001.txt'
            !print*,' > DEBUG'
            ! END DEBUGGING
            RETURN
        END IF
        
        ! Find longest chain
        DO i=1,arg_count

            largelength = length
            CALL GET_COMMAND_ARGUMENT(i, length=length)
            IF (length .gt. largelength) largelength = length
            
        ENDDO
        
        ! Parse
        allocate(character(len=largelength) :: inputfile(arg_count))
        
        DO i=1,arg_count

            CALL GET_COMMAND_ARGUMENT(i, length=length)
            IF (allocated(arg)) DEALLOCATE(arg)
            ALLOCATE(character(len=length) :: arg)
            CALL GET_COMMAND_ARGUMENT(i, arg)

            IF (arg == "-h" .or. arg == "--help") THEN
                CALL print_help()
                STOP
            END IF

            inputfile(i) = arg
            
        ENDDO
        
    END SUBROUTINE parse_commandline
    
    ! Read input file(s)
    SUBROUTINE read_inputfile(filename,head,main)
        ! DEPENDENCIES
        USE, INTRINSIC :: iso_fortran_env, ONLY : error_unit
        USE types
        USE methods_system
        ! PARAMETERS
        character(len=11) :: trash
        CHARACTER(len=:), ALLOCATABLE  :: columninfo
        ! VARIABLES
        ! io
        CHARACTER(*),                  INTENT(IN)  :: filename    !! Name of file to read
        TYPE(header),                  INTENT(OUT) :: head        !! Rows of header
        TYPE(scdata),                  INTENT(OUT) :: main        !! Rows of data
        ! local
        INTEGER :: iostat    ! Flag for open
        INTEGER :: inputfile ! Unit number for input file
        INTEGER :: i,j       ! counting variable for file lines, for doo loops
        INTEGER :: i_head    ! length of header
    
        !Open Input File
        print*, "Try"
        print*, filename
        iostat = 0
        OPEN(file=trim(filename), status="old", iostat=iostat, newunit=inputfile)

            ! Check if input file opened
            IF (iostat /= 0) THEN
                WRITE(error_unit, '(A,A,A)') "Could not open input file '", filename, "'"
                CALL exit_chchd("Could not open input file ")
            END if

            ! Process header to meta info
            read(inputfile, '(A11,I4)')    trash, head%metal
            read(inputfile, '(A11,I6)')    trash, head%N_iks 
            read(inputfile, '(A11,E11.4)') trash, head%dt
            read(inputfile, '(A11,I6)')    trash, head%Nb_Th
            read(inputfile, '(A11,I6)')    trash, head%diag_s
            read(inputfile, '(A11,I6)')    trash, head%diag_f
            read(inputfile, '(A11,E11.4)') trash, head%t_laser
            read(inputfile, '(A11,E11.4)') trash, head%E_laser
            read(inputfile, '(A11,E11.4)') trash, head%C_abs
            read(inputfile, '(A11,E11.4)') trash, head%w_laser
            read(inputfile, '(A11,E11.4)') trash, head%d_laser
            read(inputfile, '(A11,E11.4)') trash, head%d_targe
            read(inputfile, '(A11,E11.4)') trash, head%e_targe
            read(inputfile, '(A12,L1)')    trash, head%phi_E_plugged
            read(inputfile, '(A11,E11.4)')    trash, head%T_hot_e
            read(inputfile, '(A11,E11.4)')    trash, head%N_tot
            read(inputfile, '(A11,E11.4)')    trash, head%t_life
            read(inputfile, '(A11,E11.4)')    columninfo
            
            i_head = 18
            
            ! Count main data rows
            i = 0
            DO
                read(inputfile,*,iostat=iostat)
                IF (iostat /= 0) EXIT
                i = i + 1
            ENDDO
            
            ! Allocate containers for data
            CALL scdata_allocate(main,i)
            
            ! Process main data rows
            REWIND(inputfile)
            DO j = 1,i_head
                read(inputfile,*)
            ENDDO
            DO j = 1,i - i_head
                read(inputfile,'(11ES14.4E3)') main%time(j), main%radius(j), main%lambdaD(j), main%Efr(j), &
                                             & main%Ere(j), main%N(j), main%pot_Th(j), main%pot_E(j), &
                                             & main%J_fr(j), main%J_re(j), main%Q(j)
            ENDDO
            
        CLOSE(inputfile)
        
    END SUBROUTINE read_inputfile

    SUBROUTINE print_help()
        !! Print a useful help message

        PRINT *, "PAFIN: 3D Particle tracking with field interaction"
        PRINT *, ""
        PRINT *, "Usage: ./pafin [inputfile]"
    END SUBROUTINE print_help
    
    ! OUTPUT MANAGEMENT
    ! set up output file
    SUBROUTINE prepare_outputfile(filename,fileunit)
        ! DEPENDENCIES
        USE, INTRINSIC :: iso_fortran_env, ONLY : error_unit
        USE methods_system
        ! PARAMETERS
        ! io
        CHARACTER(*), INTENT(IN)  :: filename    !! Name of file to read
        INTEGER,      INTENT(OUT) :: fileunit    !! new unit number of file
        ! local
        INTEGER :: iostat    !! Flag for open
        ! OPEN AND WRITE HEADER
        print*, "Try"
        print*, trim(filename)
        iostat = 0
        OPEN(file=trim(filename), status="new", iostat=iostat, newunit=fileunit)

            ! Check if input file opened
            IF (iostat /= 0) THEN
                WRITE(error_unit, '(A,A,A)') "Could not open file '", filename, "'"
                CALL exit_chchd("Could not open file ")
            END if
        
    END SUBROUTINE
	
	! set up pyplot file
    SUBROUTINE write_pyplot(filename)
        ! DEPENDENCIES
        USE, INTRINSIC :: iso_fortran_env, ONLY : error_unit
        USE methods_system
        ! PARAMETERS
        ! io
        CHARACTER(*), INTENT(IN)  :: filename    !! Name of file to read
        ! local
        INTEGER :: fileunit    !! new unit number of file
        INTEGER :: iostat      !! Flag for open
        ! OPEN AND WRITE HEADER
        print*, "Try"
        print*, trim(filename)
        iostat = 0
        OPEN(file=trim(filename), status="replace", iostat=iostat, newunit=fileunit)

            ! Check if input file opened
            IF (iostat /= 0) THEN
                WRITE(error_unit, '(A,A,A)') "Could not open file '", filename, "'"
                CALL exit_chchd("Could not open file ")
            END if

            ! write script
            write(fileunit,'(a)') "import sys"
            write(fileunit,'(a)') "import numpy as np"
            write(fileunit,'(a)') "import matplotlib.pyplot as plt"
            write(fileunit,'(a)') "from collections import namedtuple as nt"
            
            write(fileunit,'(a)') "arg_names = ['script','inputfile1','operator','inputfile2']"
            write(fileunit,'(a)') "args = dict(zip(arg_names, sys.argv))"

            write(fileunit,'(a)') "Arg_list = nt('Arg_list', arg_names)"
            write(fileunit,'(a)') "args = Arg_list(*(args.get(arg, None) for arg in arg_names))"
            
            write(fileunit,'(a)') "data = np.genfromtxt(args[1], comments='#')"
            write(fileunit,'(a)') "with open(args[1]) as f:"
            write(fileunit,'(a)') "    firstline = f.readline().strip().split('#')"
            
            write(fileunit,'(a)') "if args[3] != None:"
            write(fileunit,'(a)') "    data2 = np.genfromtxt(args[3], comments='#')"
            write(fileunit,'(a)') "    with open(args[3]) as f:"
            write(fileunit,'(a)') "        firstline2 = f.readline().strip().split('#')"
            
            write(fileunit,'(a)') "fig, ax = plt.subplots()"
            write(fileunit,'(a)') "ax.plot(data[:,0], data[:,1])"
            write(fileunit,'(a)') "ax.scatter(data[:,0], data[:,1])"
            write(fileunit,'(a)') "if args[2] != None:"
            write(fileunit,'(a)') "    if args[2] == '+':"
            write(fileunit,'(a)') "        ax.plot(data2[:,0], data2[:,1])"
            write(fileunit,'(a)') "        ax.scatter(data2[:,0], data2[:,1])"
            write(fileunit,'(a)') "        ax.plot(data2[:,0], data[:,1]+data2[:,1])"
            write(fileunit,'(a)') "        ax.scatter(data2[:,0], data[:,1]+data2[:,1])"
            write(fileunit,'(a)') "    elif args[2] == '-':"
            write(fileunit,'(a)') "        ax.plot(data2[:,0], data2[:,1])"
            write(fileunit,'(a)') "        ax.scatter(data2[:,0], data2[:,1])"
            write(fileunit,'(a)') "        ax.plot(data2[:,0], data[:,1]-data2[:,1])"
            write(fileunit,'(a)') "        ax.scatter(data2[:,0], data[:,1]-data2[:,1])"
            write(fileunit,'(a)') "    else:"
            write(fileunit,'(a)') "        ax.plot(data2[:,0], data2[:,1])"
            write(fileunit,'(a)') "        ax.scatter(data2[:,0], data2[:,1])"
            write(fileunit,'(a)') "    ax.set(xlabel=firstline[1]+', '+firstline2[1], " // &
                                  &   "ylabel=firstline[2]+', '+firstline2[2], title='')"
            write(fileunit,'(a)') "else:"
            write(fileunit,'(a)') "    ax.set(xlabel=firstline[1], ylabel=firstline[2], title='')"
            write(fileunit,'(a)') "ax.grid()"
            write(fileunit,'(a)') "plt.show()"
            write(fileunit,'(a)') "plt.close('all')"
        
        CLOSE(fileunit)
        
    END SUBROUTINE
    
    ! close any file
    SUBROUTINE close_file(fileunit)
        ! PARAMETERS
        ! io
        INTEGER,      INTENT(IN) :: fileunit    !! new unit number of file
        ! DO
        CLOSE(fileunit)
    END SUBROUTINE

END MODULE methods_io

MODULE methods_math
    ! DEPENDENCIES
    USE methods_system, ONLY: q
    ! VARIABLES
    IMPLICIT none
    
CONTAINS
    ! ROLLING AVERAGE
    ! original mathod from Dr. Ching-Kuang Shene shene@mtu.edu, modified to ALLOCATABLE and REAL(kind=q),
    ! source on https://pages.mtu.edu/~shene/COURSES/cs201/NOTES/chap08/mov-avg.html 12/2021
    ! modified to be a central average
    SUBROUTINE MovingAverage(x,Wing)
        ! VARIABLES
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: x(:)        ! input array
        INTEGER, INTENT(IN)                      :: Wing        ! wing size
        ! OUT
        REAL(kind=q), ALLOCATABLE             :: Avg(:)        ! arrays
        ! INTERNAL
        REAL(kind=q)             :: aSum                ! for computation use            
        INTEGER                  :: aSize               ! actual array size
        INTEGER                  :: i, j                ! indices
        ! MAIN
        aSize = size(x)
        IF(allocated(Avg)) DEALLOCATE(Avg)
        ALLOCATE(Avg(aSize-2*Wing+1))
        DO i = Wing+1, aSize-Wing                  ! for each xi
            aSum = 0.0                             ! compute the moving average
            DO j = i-Wing, i+Wing                  ! of xi, x(i+1), ...,
                aSum = aSum + x(j)                 ! x(i+Wing-1).
            END DO
            Avg(i-Wing) = aSum / real(2 * Wing + 1,kind=q)     ! save the result
        END DO
        x(Wing+1:aSize-Wing) = Avg(1:size(Avg)-1)
        ! END MAIN
    END SUBROUTINE MovingAverage
    ! GAUSSIAN KERNEL
    ! according to hearsay-standards
    SUBROUTINE GaussianKernel(x,Wing)
        ! VARIABLES
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: x(:)        ! input array
        INTEGER, INTENT(IN)                      :: Wing        ! wing size
        ! OUT
        REAL(kind=q), ALLOCATABLE             :: Avg(:)        ! arrays
        ! INTERNAL
        INTEGER                  :: aStd                ! standard deviation
        REAL(kind=q)             :: aSum                ! for computation use            
        INTEGER                  :: aSize               ! actual array size
        INTEGER                  :: i, j                ! indices
        ! MAIN
        aStd = int(sqrt((real(2*Wing,kind=q)**2-1.)/12.))
        aSize = size(x)
        IF(allocated(Avg)) DEALLOCATE(Avg)
        ALLOCATE(Avg(aSize-2*Wing+1))
        DO i = Wing+1, aSize-Wing                                       ! for each xi
            aSum = 0.0                                                  ! compute the gausz
            DO j = i-Wing, i+Wing                                       ! of xi, x(i+1), ...,
                aSum = aSum + x(j) * exp(-0.5*((j-i)/aStd)**2)    ! x(i+Wing-1).
            END DO
            Avg(i-Wing) = aSum / (real(aStd,kind=q)*3.1415*sqrt(2.))     ! save the result
        END DO
        x(Wing+1:aSize-Wing) = Avg(1:size(Avg)-1)
        ! END MAIN
    END SUBROUTINE GaussianKernel
    ! INTERPOLATE ARRAYS
    ! linear 1D real
    SUBROUTINE interpolate1Dreal(array,add_intermediate_steps,method)
        ! DEPENDENCIES
        USE methods_system
        ! PARAMETERS
        ! io
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: array(:)               !! 1D array to expandSteps_factor
        INTEGER, INTENT(IN)                :: add_intermediate_steps !! number of new intermediate steps
        CHARACTER(*), OPTIONAL, INTENT(IN) :: method                 !! interpolation types
        ! local
        INTEGER             :: i,j,N
        REAL(kind=q), ALLOCATABLE :: array_copy(:),array_slice(:)
        REAL(kind=q)              :: past_array_value, next_array_value, stepper
        ! MAIN
        ! evaulate size of new array
        N = (size(array)-1) * (add_intermediate_steps+1)
        ! copy content
        IF(allocated(array_copy)) DEALLOCATE(array_copy)
        ALLOCATE(array_copy(size(array)))
        array_copy = array
        ! reallocate array
        DEALLOCATE(array)
        ALLOCATE(array(N))
        ! define array content
        IF (.not.present(method) .or. trim(method).eq.'linear') THEN
            ! LINEAR INTERPOLATION (is default)
            ! loop over cells delimited by old array values
            DO i=1,size(array_copy)-1
                ! set lower boundary
                past_array_value = array_copy(i)
                ! update upper boundary
                next_array_value = array_copy(i+1)
                ! calculate incremental step
                stepper = (next_array_value - past_array_value) / real(add_intermediate_steps+1,kind=q)
                ! fill new array
                DO j=0,add_intermediate_steps
                    array((i-1)*(add_intermediate_steps+1)+1+j) = stepper * real(j,kind=q) + past_array_value
                END DO
                ! write higher boundary in last step
                IF (i.eq.size(array_copy)-1) array(size(array)) = array_copy(size(array_copy))
            END DO
        ELSE IF (trim(method).eq.'newton') THEN
            ! NEWTON METHOD ON SLICES OF 5 FOR NONCONSTAND d_t^2 A
            DO i=1,size(array_copy)-5
                IF(allocated(array_slice)) DEALLOCATE(array_slice)
                ALLOCATE(array_slice(5))
                array_slice = array_copy(i:i+4)
                CALL interpolate1Dreal_NDDIP(array_slice,add_intermediate_steps)
                DO j=0,add_intermediate_steps
                    array((i-1)*(add_intermediate_steps+1)+1+j) = array_slice(j+1)
                END DO
            END DO
            array((size(array_copy)-6)*(add_intermediate_steps+1):size(array)) = array_slice
        ELSE
            CALL exit_chchd("Unknown interpolation method ")
        END IF
        ! END MAIN
    END SUBROUTINE
    ! Newton's devided difference interpolating polynomials 1D real
    ! inspired by http://users.metu.edu.tr/csert/me310/me310_6_interpolation.pdf
    SUBROUTINE interpolate1Dreal_NDDIP(array,add_intermediate_steps)
        ! DEPENDENCIES
        USE methods_system
        ! PARAMETERS
        ! io
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: array(:)         !! 1D array to expandSteps_factor
        INTEGER, INTENT(IN)                :: add_intermediate_steps !! number of new intermediate steps
        ! local
        INTEGER             :: i,j,k,N,M
        REAL(kind=q), ALLOCATABLE :: array_copy(:),coefficients(:),polynomials(:)
        REAL(kind=q)              :: x_position,y_val,zeros
        ! MAIN
        ! find feasible order of interpolation
        N = size(array) - 1
        ! determine coefficients recursively
        ! note array ranges from 1 are coefficient ranges from 0
        ! presume grid spacing of one for array points
        IF(allocated(coefficients)) DEALLOCATE(coefficients)
        ALLOCATE(coefficients(N+1))
        coefficients = array
        DO i=1,N
            DO j=N,i,-1
                coefficients(j+1) = (coefficients(j+1) - coefficients(j))/(real(i,kind=q)) 
            END DO
        END DO
        !print*,"  * DID  COEFFICIENTS"
        ! calculate all intermediate steps
        M = (size(array)-1) * (add_intermediate_steps+1) + 1
        IF(allocated(array_copy)) DEALLOCATE(array_copy)
        ALLOCATE(array_copy(N+1))
        array_copy = array
        DEALLOCATE(array)
        ALLOCATE(array(M))
        IF (allocated(polynomials)) DEALLOCATE(polynomials)
        ALLOCATE(polynomials(N))
        DO i=1,M
            !print*,"DO VAL",i
            x_position = real(i-1,kind=q) * real(N,kind=q)/real(M-1,kind=q) + 1.
            IF (mod(i-1,add_intermediate_steps+1) .eq. 0) THEN
                array(i) = array_copy(int(x_position))
                CYCLE
            END IF
            DO j=1,N
                polynomials(j) = x_position-real(j,kind=q)
            END DO
            y_val = coefficients(1)
            DO j=N+1,2,-1
                zeros = 1.
                DO k = j-1,1,-1
                    zeros = zeros * polynomials(k)
                    !IF (zeros .eq. 0.) EXIT
                END DO
                y_val = y_val + coefficients(j) * zeros
            END DO
            array(i) = y_val
        END DO
        !print*,"  * DID  VALUES"
        ! END MAIN
    END SUBROUTINE interpolate1Dreal_NDDIP
    
END MODULE methods_math

! #############################################################
! #############################################################
! HEAD: PREPARE CONTENT MODULES
! #############################################################

MODULE variables_input
    ! DEPENDENCIES
    USE types
    USE methods_system, ONLY: q
    ! VARIABLES
    IMPLICIT none
    !> rows of input file
    TYPE(header), PUBLIC :: chocolat_meta
    TYPE(scdata), PUBLIC :: chocolat_scalar
    !> conductivity library in Ohms per metre
    !  (1:carbone 2:aluminum 3:cuivre 4:tungstene 5:tantale 6:plomb, 7:gold, 8:silver, 9:teflon)
    REAL(kind=q), DIMENSION(9), PUBLIC :: resistivity_SI = (/7837.e-9, 26.5e-9, 16.8e-9, 52.8e-9, 131.e-9,&
                                                       & 208.e-9, 22.14e-9, 15.9e-9, 5.e23/)
    !> parameters
    REAL(kind=q), PUBLIC :: vacuum_permittivity_SI = 8.85418781e-12
    REAL(kind=q), PUBLIC :: vacuum_permeability_SI = 1.256637062e-6
    REAL(kind=q), PUBLIC :: speed_of_light_SI = 299792458.
    REAL(kind=q), PUBLIC :: s_to_ps = 1.e12
    REAL(kind=q), PUBLIC :: s_to_fs = 1.e15
    REAL(kind=q), PUBLIC :: ps_to_s = 1.e-12
    REAL(kind=q), PUBLIC :: m_to_mm = 1.e3
    REAL(kind=q), PUBLIC :: mm_to_m = 1.e-3
    REAL(kind=q), PUBLIC :: um_to_mm = 1.e-3
    REAL(kind=q), PUBLIC :: um_to_m = 1.e-6
    REAL(kind=q), PUBLIC :: nC_to_C = 1.e-9
    REAL(kind=q), PUBLIC :: C_to_nC = 1.e9
    REAL(kind=q), PUBLIC :: pi = 4.*atan(1.)
    !> normalization
    REAL(kind=q), PUBLIC :: Q_over_nQ = 1.602176634e-16                !! charges normalized to one thousand elementary charges
    REAL(kind=q), PUBLIC :: T_over_nT = 1.e-15                         !! times normalized to 1fs
    REAL(kind=q), PUBLIC :: R_over_nR = 299.792458e-9                  !! distances normalized to event horizon in 1fs
    REAL(kind=q), PUBLIC :: M_over_nM = 9.10938370e-31                 !! masses normalized to electron mass
    !> settings
    !LOGICAL, PUBLIC :: use_hot_electron_field = .true.
    LOGICAL, PUBLIC :: messages = .false.
    LOGICAL, PUBLIC :: export_stepwise = .true.
    INTEGER, PUBLIC :: export_stepwise_ith = 100
    REAL(kind=q) , PUBLIC :: timescale = 10.e-12
    INTEGER, PUBLIC :: expandSteps_factor = 0                !! calculate this amount of intermediate steps and adjust space step
    LOGICAL, PUBLIC :: decrese_spacestep = .true.            !! .. then maintain CFL constant
	REAL(kind=q), PUBLIC :: spatial_resolution_factor = 0.1  !! divide space step by this value to modify CLF
END MODULE variables_input

MODULE methods_physics
    ! DEPENDENCIES
    USE methods_system, ONLY: q
    USE variables_input, ONLY: vacuum_permeability_SI, vacuum_permittivity_SI, &
                             & resistivity_SI, messages, pi
    USE methods_math
    ! VARIABLES
    IMPLICIT none
CONTAINS
    ! KNOWN ELECTRIC FIELDS
    ! radial field of finite line of charge (next to the line!)
    !FUNCTION Er_lineofcharge(r,z_a,z_b,lambda)
    !    ! DEPENDENCIES
    !    USE variables_input, ONLY: vacuum_permittivity_SI, pi
    !    ! VARIABLES
    !    IMPLICIT none
    !    REAL(kind=q), INTENT(IN) :: r                   !! radial position of observer seen from line of charge
    !    REAL(kind=q), INTENT(IN) :: z_a,z_b             !! horizontal plane of observer defined with both distances to line's end
    !    REAL(kind=q), INTENT(IN) :: lambda              !! linear charge distribution
    !    ! OUTPUT TYPE
    !    REAL(kind=q) :: Er_lineofcharge                 !! expect input units to allow output in units of V/m
    !    ! MAIN
    !    ! one liner
    !    Er_lineofcharge = lambda / (4.*pi*vacuum_permittivity_SI*r) / sqrt(0.25 + (r/(z_a+z_b)**2))
    !END FUNCTION Er_lineofcharge
    ! KNOWN CHARGE DISTRIBUTIONS
    ! radial Gausz distribution, uniform else
    FUNCTION rho_gausz(spacestep,Q0,FWHM,skin,N_max) result(distribution)
        ! DEPENDENCIES
        USE variables_input, ONLY: pi
        ! VARIABLES
        IMPLICIT none
        REAL(kind=q), INTENT(IN) :: spacestep           !! regular space increment between eval positions
        REAL(kind=q), INTENT(IN) :: Q0                  !! charge that should be distributed
        REAL(kind=q), INTENT(IN) :: FWHM                !! FWHM of distribution
        REAL(kind=q), INTENT(IN) :: skin                !! skin depth of distribution
        INTEGER,INTENT(IN)       :: N_max               !! eval points [0,N_max]
        ! INTERNAL
        REAL(kind=q)             :: kappa               !! 1/e width
        REAL(kind=q)             :: r0 = 0.             !! centre (unequal zero does create doughnut)
        INTEGER                  :: i                   !! iterator
        ! OUTPUT TYPE
        REAL(kind=q), ALLOCATABLE :: distribution(:)    !! output distribution
        ! MAIN
        ! prepare grid
        IF(allocated(distribution)) DEALLOCATE(distribution)
        ALLOCATE(distribution(N_max))
        ! fill grid
        kappa = (FWHM/2.)**2/log(2.)
        !$OMP PARALLEL SHARED(distribution,spacestep,r0,kappa,N_max) PRIVATE(i)
        !$OMP DO
        DO i=1, N_max-6
            distribution(i) = exp(-(real(i-6,kind=q)*spacestep - r0)**2/kappa)
        END DO
        !$OMP END DO
        !$OMP END PARALLEL
        ! outer boundary is reflecting too
        DO i=1, 6
            distribution(N_max-6+i) = distribution(N_max-6-i)
        END DO
        ! factors
        distribution = distribution * Q0 / kappa / skin / pi
        ! END MAIN
    END FUNCTION rho_gausz
    ! LAPLACE OPERATOR
    ! automatic version for allocatable arrays, that is central form,
    ! for the radial component in cylindrical coordinates
    SUBROUTINE laplace_r(spacestep,array_r,L_r,symetric)
        ! DEPENDENCIES
        USE methods_system
        ! VARIABLES
        IMPLICIT none
        REAL(kind=q),              INTENT(IN) :: spacestep    !! spacestep
        REAL(kind=q), ALLOCATABLE, INTENT(IN) :: array_r(:)   !! allocatable input array
        LOGICAL,                   INTENT(IN) :: symetric     !! cause symetric to origin
        INTEGER :: j,i                                        !! iterator
        ! OUTPUT TYPE
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: L_r(:)    !! laplace_r(input array)
        ! MAIN
        ! loop through array and consider boundary as von Neumann with ghost cells,
        ! and deploy l' Hopital in zero
        ! ( Dirichlet boundary (fixed)
        !   von Neumann boundary (open) )
        ! and remind that node 6 is space 0
        ! all in 8th order accuracy
        IF (size(array_r) .gt. 12) THEN
            !$OMP PARALLEL SHARED(L_r,array_r,spacestep) PRIVATE(j)
            !$OMP DO
            DO j=6,size(array_r)-6
                IF ( j .ne. 6 .and. j .ne. size(array_r) - 6 ) THEN
                    L_r(j) = (-9.*array_r(j-4)+128.*array_r(j-3)-1008.*array_r(j-2)+8064.*array_r(j-1)-14350.*array_r(j) &
                             &+8064.*array_r(j+1)-1008.*array_r(j+2)+128.*array_r(j+3)-9.*array_r(j+4)) &
                           & /(5040.*spacestep**2) + &
                           & (18.*array_r(j-4)-192.*array_r(j-3)+1008.*array_r(j-2)-4032.*array_r(j-1) &
                             &+4032.*array_r(j+1)-1008.*array_r(j+2)+192.*array_r(j+3)-18.*array_r(j+4)) &
                           & / (real(j-6,kind=q)*spacestep) &
                           & /(5040.*spacestep)
                ELSE
                    L_r(j) = (-9.*array_r(j-4)+128.*array_r(j-3)-1008.*array_r(j-2)+8064.*array_r(j-1)-14350.*array_r(j) &
                             &+8064.*array_r(j+1)-1008.*array_r(j+2)+128.*array_r(j+3)-9.*array_r(j+4)) &
                           & /(5040.*spacestep**2) * &
                           & 2.
                END IF
            END DO
            !$OMP END DO
            !$OMP END PARALLEL
        ELSE
            CALL exit_chchd("Not enough spatial cells for this order laplacian ")
        END IF
        ! copy nodes and half nodes to boundary margin
        DO j=1,6
            IF (symetric) THEN
                ! inner
                L_r(6-(j-1)) = L_r(6+(j-1))
                ! outer
                L_r(size(array_r)-6+j) = L_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
            ELSE
                ! inner
                L_r(6-(j-1)) = - L_r(6+(j-1))
                ! outer
                L_r(size(array_r)-6+j) = - L_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
            END IF
        ENDDO
        ! smoothing
        DO i=0,2
            CALL GaussianKernel(L_r,6-1)
            DO j=1,6
                IF (symetric) THEN
                    ! inner
                    L_r(6-(j-1)) = L_r(6+(j-1))
                    ! outer
                    L_r(size(array_r)-6+j) = L_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
                ELSE
                    ! inner
                    L_r(6-(j-1)) = - L_r(6+(j-1))
                    ! outer
                    L_r(size(array_r)-6+j) = - L_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
                END IF
            ENDDO
        ENDDO
    END SUBROUTINE laplace_r
    ! 1D PARTIAL DERIVATIVE OPERATOR
    ! automatic version for allocatable arrays, that is central form,
    ! for the radial component in cylindrical coordinates
    SUBROUTINE oneD_partial(spacestep,array_r,p_r,symetric)
        ! DEPENDENCIES
        USE methods_system
        ! VARIABLES
        IMPLICIT none
        REAL(kind=q),              INTENT(IN) :: spacestep    !! spacestep
        REAL(kind=q), ALLOCATABLE, INTENT(IN) :: array_r(:)   !! allocatable input array
        LOGICAL,                   INTENT(IN) :: symetric     !! cause symetric to origin
        INTEGER :: j,i                                        !! iterator
        ! OUTPUT TYPE
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: p_r(:)    !! partial derivative (input array)
        ! MAINsymetric
        ! loop through array and consider boundary as von Neumann with ghost cells,
        ! and deploy l' Hopital in zero
        ! ( Dirichlet boundary (fixed)
        !   von Neumann boundary (open) )
        ! and remind that node 6 is space 0
        ! all in 8th order accuracy
        !$OMP PARALLEL SHARED(p_r,array_r,spacestep) PRIVATE(j)
        !$OMP DO
        DO j=6,size(array_r)-6
            p_r(j) = (18.*array_r(j-4)-192.*array_r(j-3)+1008.*array_r(j-2)-4032.*array_r(j-1) &
                             &+4032.*array_r(j+1)-1008.*array_r(j+2)+192.*array_r(j+3)-18.*array_r(j+4)) &
                       & /(5040.*spacestep)
        END DO
        !$OMP END DO
        !$OMP END PARALLEL
        ! MAYBE fix to avoid numerical glitches
        !p_r(6) = 0.
        !p_r(size(array_r)-6) = 0.
        ! copy nodes and half nodes to boundary margin with jump in sign due to extremum implied by reflecting boundary
        DO j=1,6
            IF (symetric) THEN
                ! inner
                p_r(6-(j-1)) = - p_r(6+(j-1))
                ! outer
                p_r(size(array_r)-6+j) = - p_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
            ELSE
                ! inner
                p_r(6-(j-1)) = p_r(6+(j-1))
                ! outer
                p_r(size(array_r)-6+j) = p_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
            END IF
        ENDDO
        ! smoothing
        DO i=0,2
            CALL GaussianKernel(p_r,6-1)
            DO j=1,6
                IF (symetric) THEN
                    ! inner
                    p_r(6-(j-1)) = - p_r(6+(j-1))
                    ! outer
                    p_r(size(array_r)-6+j) = - p_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
                ELSE
                    ! inner
                    p_r(6-(j-1)) = p_r(6+(j-1))
                    ! outer
                    p_r(size(array_r)-6+j) = p_r(size(array_r)-6-j)  !! should the outer boundary be continouus or mirrored? TODO
                END IF
            ENDDO
        ENDDO
   END SUBROUTINE oneD_partial
   ! MAXWELL-OHM ITERATION STEP IN TIME DOMAIN
   ! potential wave equation in radial cylindrical coordinate in Lorenz gauge
   SUBROUTINE timestep_potential(resistivity,permeability,permittivity,one_over_c_squared,&
                                &timestep,spacestep,Phi,rho,change_rho,A_r,J_r)
        ! DEPENDENCIES
        USE methods_math
        ! VARIABLES
        IMPLICIT none
        REAL(kind=q),              INTENT(IN)    :: resistivity        !! of material
        REAL(kind=q),              INTENT(IN)    :: permeability       !! of material
        REAL(kind=q),              INTENT(IN)    :: permittivity       !! of material
        REAL(kind=q),              INTENT(IN)    :: one_over_c_squared !! c-2 = mu epsilon
        REAL(kind=q),              INTENT(IN)    :: timestep           !! timestep
        REAL(kind=q),              INTENT(IN)    :: spacestep          !! spacestep
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: Phi(:,:)           !! electric potential in (t_1-k,r_i)
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: rho(:)             !! charge density in (r_i)
        REAL(kind=q), ALLOCATABLE, INTENT(IN)    :: change_rho(:)      !! imposed change to charge density in (r_i)
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: A_r(:,:)           !! vector potential in (t_1-k,r_i)
        REAL(kind=q), ALLOCATABLE                :: L_r(:)             !! results to radial lapplace operator in (r_i)
        REAL(kind=q), ALLOCATABLE                :: p_r(:)             !! results to 1D partial operator in (r_i)
        REAL(kind=q), ALLOCATABLE                :: A_r_dividedby_r(:) !! results to A_r/r**2 (r_i)
        INTEGER                            :: i,j                  !! iterator
        REAL(kind=q), ALLOCATABLE, INTENT(OUT)   :: J_r(:)             !! vector potential in (t_1-k,r_i)
        REAL(kind=q), ALLOCATABLE                :: J_r_dividedby_r(:) !! results to J_r/r (r_i)
        REAL(kind=q), ALLOCATABLE                :: Phi_now_dummy(:)   !! allocatable dummy for slices
        REAL(kind=q), ALLOCATABLE                :: Phi_next_dummy(:)   !! allocatable dummy for slices
        REAL(kind=q), ALLOCATABLE                :: A_r_now_dummy(:)   !! allocatable dummy for slices
        REAL(kind=q), ALLOCATABLE                :: A_r_next_dummy(:)  !! allocatable dummy for slices
		REAL(kind=q), ALLOCATABLE                :: dt_A_r_dummy(:)    !! allocatable dummy for slices
        ! MAIN
        IF (messages) print*, '    - UPDATE'
        ! change rho by externally applied changes
        rho = rho + change_rho
        IF (messages) print*, '      DID ADD TO RHO'
        ! move electric potential Phi to past
        Phi(2:8,:) = Phi(1:7,:)
        IF (messages) print*, '      DID SHIFT PHI'
        ! calculate radial laplacian (cylincrical system) of Phi in t_0,r_i
        IF (allocated(L_r)) DEALLOCATE(L_r)
        ALLOCATE(L_r(size(Phi(1,:))))
        IF (allocated(Phi_now_dummy)) DEALLOCATE(Phi_now_dummy)
        ALLOCATE(Phi_now_dummy(size(Phi(1,:))))
        Phi_now_dummy = Phi(2,:)
        ! smoothing
        !DO j=0,2
        !    CALL GaussianKernel(Phi_now_dummy,6-1)
        !    DO i=1,6
        !        ! inner
        !        Phi_now_dummy(6-(i-1)) = Phi_now_dummy(6+(i-1))
        !        ! outer
        !        Phi_now_dummy(size(rho)-6+i) = Phi_now_dummy(size(rho)-6-i)
        !    END DO
        !ENDDO
        ! derivative
        CALL laplace_r(spacestep,Phi_now_dummy,L_r,.true.)
		IF (messages) print*, '      ', L_r(1:11)
        IF (messages) print*, '      DID LAPLACIAN DERIVATIVE'
        ! future potential by potential wave equation, 5-point discretized
        ! according to S. Latif et al., Journal of Mathematical Science & Computational Mathematics, 2, 3 (2021)
        Phi(1,:) = ( (L_r+rho/permittivity)/one_over_c_squared * 12.*timestep**2 - &
                 &   (Phi(6,:)-6.*Phi(5,:)+14.*Phi(4,:)-4.*Phi(3,:)-15*Phi(2,:)) ) &
                 & /10.
        ! HARDFIX counter oscillating solution
		! note: ([n+1]+[n-1]+2[n])/4 instead n 
		!       =>  try to cure instability by mean (motivated by von Neumann stability analysis)
		Phi(1,2:size(Phi(1,:))-1) = 0.25 * (Phi(1,1:size(Phi(1,:))-2) + 2.* Phi(1,2:size(Phi(1,:))-1) + Phi(1,3:size(Phi(1,:))))
		Phi(1,1) = Phi(1,11)
        Phi(1,size(Phi(1,:))) = Phi(1,size(Phi(1,:))-11)
        IF (messages) print*, '      DID NEW PHI'
        ! smoothing
        !IF (allocated(Phi_next_dummy)) DEALLOCATE(Phi_next_dummy)
        !ALLOCATE(Phi_next_dummy(size(Phi(1,:))))
        !Phi_next_dummy = Phi(1,:)
        !DO j=0,2
        !    CALL GaussianKernel(Phi_next_dummy,6-1)
        !    DO i=1,6
        !        ! inner
        !        Phi_next_dummy(6-(i-1)) = Phi_next_dummy(6+(i-1))
        !        ! outer
        !        Phi_next_dummy(size(rho)-6+i) = Phi_next_dummy(size(rho)-6-i)
        !    END DO
        !ENDDO
        !Phi(1,:) = Phi_next_dummy(:)
        ! move radial component of vector potential A to past
        A_r(2:8,:) = A_r(1:7,:)
        IF (messages) print*, '      DID SHIFT A'
        ! calculate radial laplacian (cylindrical system) of A_r in t_0,r_i+0.5
        IF (allocated(A_r_now_dummy)) DEALLOCATE(A_r_now_dummy)
        ALLOCATE(A_r_now_dummy(size(A_r(1,:))))
        A_r_now_dummy = A_r(2,:)
        CALL laplace_r(spacestep,A_r_now_dummy,L_r,.false.)
		IF (messages) print*, '      ', L_r(1:11)
        IF (messages) print*, '      DID LAPLACIAN DERIVATIVE'
        ! calculate radial partial diff (cylindrical system) of Phi in t_0,r_i+0.5
        IF (allocated(p_r)) DEALLOCATE(p_r)
        ALLOCATE(p_r(size(Phi(1,:))))
        CALL oneD_partial(spacestep,Phi_now_dummy,p_r,.true.)
		IF (messages) print*, '      ', p_r(1:11)
        IF (messages) print*, '      DID PARTIAL DERIVATIVE'
        ! store in obsolete component of Phi
        Phi(8,:) = p_r
        ! calculate A_r/r**2 for t_0,r_i,
        ! with l'Hopital in 0, where von Neumann boundary is applied
        ! remind that node 6 is space 0
		! note: ([n+1]+[n-1])/2 instead n 
		!       =>  try to cure instability by mean (motivated by von Neumann stability analysis)
        IF (allocated(A_r_dividedby_r)) DEALLOCATE(A_r_dividedby_r)
        ALLOCATE(A_r_dividedby_r(size(A_r(1,:))))
        !$OMP PARALLEL SHARED(A_r_dividedby_r,A_r,spacestep) PRIVATE(i)
        !$OMP DO
        DO i=2,size(A_r(1,:)-1)
            IF (i .ne. 6 .and. i .ne. size(A_r(1,:)-6)) THEN
                A_r_dividedby_r(i) = (A_r(2,i+1) + A_r(2,i-1))/2.&
				                   & / ((real(i-6,kind=q)+0.)*spacestep)**2
            ELSE
                A_r_dividedby_r(i) = 0. !0.5 * ( (-A_r(2,i-2)+16.*A_r(2,i-1)-30.*A_r(2,i)+16.*A_r(2,i+1)-A_r(2,i+2)) &
                                        !   & /(12.*spacestep**2) ) !! is zero due to symetry <> avoid numerical glitches
            END IF
        END DO
        !$OMP END DO
        !$OMP END PARALLEL
		A_r_dividedby_r(1) = A_r_dividedby_r(11)
        DO i = 1,6
            A_r_dividedby_r(size(A_r_dividedby_r)-6+i) = A_r_dividedby_r(size(A_r_dividedby_r)-6-i)
        END DO
		IF (messages) print*, '      ', A_r_dividedby_r(1:11)
        IF (messages) print*, '      DID RADIAL'
        ! TODO delete the following line
        !Phi(6,:) = A_r_dividedby_r
        ! future vector potential by vector potential wave equation, with current density
        ! replaced by Ohm's law and the electric field then by it's differential form in potentials,
        ! and use 4th order accuracy aside-backward finite difference for second temporal derivative
        ! according to S. Latif et al., Journal of Mathematical Science & Computational Mathematics, 2, 3 (2021)
        A_r(1,:) = (L_r - permeability / resistivity * p_r - A_r_dividedby_r - &
                 &  one_over_c_squared * ( A_r(6,:)-6.*A_r(5,:)+14.*A_r(4,:)-4.*A_r(3,:)-15.*A_r(2,:) ) &
                 &  /(12.*timestep**2) + &
                 &  permeability / resistivity /(2.*timestep) * A_r(3,:)  ) &
                 & /( 10.*one_over_c_squared/(12.*timestep**2) + &
                 &    permeability / resistivity /(2.*timestep) )
        IF (messages) print*, '      DID NEW A'
		! HARDFIX divergent A(6) and A(-6)
		A_r(1,6) = 0.
		A_r(1,size(A_r(1,:))-6) = 0.
		! HARDFIX counter oscillating solution
		! note: ([n+1]+[n-1]+2[n])/4 instead n 
		!       =>  try to cure instability by mean (motivated by von Neumann stability analysis)
		A_r(1,2:size(A_r(1,:))-1) = 0.25 * (A_r(1,1:size(A_r(1,:))-2) + 2.* A_r(1,2:size(A_r(1,:))-1) + A_r(1,3:size(A_r(1,:))))
		A_r(1,1) = - A_r(1,11)
        A_r(1,size(A_r(1,:))) = - A_r(1,size(A_r(1,:))-11)
        ! smoothing
        !IF (allocated(A_r_next_dummy)) DEALLOCATE(A_r_next_dummy)
        !ALLOCATE(A_r_next_dummy(size(A_r(1,:))))
        !A_r_next_dummy = A_r(1,:)
        !DO j=0,2
        !    CALL GaussianKernel(A_r_next_dummy,6-1)
        !    DO i=1,6
        !        ! inner
        !        A_r_next_dummy(6-(i-1)) = - A_r_next_dummy(6+(i-1))
        !        ! outer
        !        A_r_next_dummy(size(rho)-6+i) = - A_r_next_dummy(size(rho)-6-i)
        !    END DO
        !ENDDO
        !A_r(1,:) = A_r_next_dummy(:)
        ! actual current density (between past and future)
        ! by Ohm's law, the electric field then by it's differential form in potentials
        ! and use 6th order accuracy backward finite difference for first temporal derivative
		! calculate dt A
		IF (allocated(dt_A_r_dummy)) DEALLOCATE(dt_A_r_dummy)
		ALLOCATE (dt_A_r_dummy(size(A_r(8,:))))
		dt_A_r_dummy(:) = (147.*A_r(2,:)-360.*A_r(3,:)+450.*A_r(4,:)-400.*A_r(5,:)+225.*A_r(6,:)-72.*A_r(7,:)+10.*A_r(8,:)) &
                        & /(60.*timestep)
	    ! smoothing
        DO j=0,2
            CALL GaussianKernel(dt_A_r_dummy,6-1)
            DO i=1,6
                ! inner
                dt_A_r_dummy(6-(i-1)) = - dt_A_r_dummy(6+(i-1))
                ! outer
                dt_A_r_dummy(size(rho)-6+i) = - dt_A_r_dummy(size(rho)-6-i)
            END DO
        ENDDO
		! calculate J
        IF (allocated(J_r)) DEALLOCATE(J_r)
        ALLOCATE(J_r(size(A_r(1,:))))
        J_r = - (p_r + dt_A_r_dummy)/resistivity
        IF (messages) print*, '      DID ACTUAL J'
        ! smoothing
        DO j=0,2
            CALL GaussianKernel(J_r,6-1)
            DO i=1,6
                ! inner
                J_r(6-(i-1)) = - J_r(6+(i-1))
                ! outer
                J_r(size(rho)-6+i) = - J_r(size(rho)-6-i)
            END DO
        ENDDO
        ! START REMOVE TODO
        !print*, J_r(1:12)
        ! force symetry to J_r TODO seems not necessary
        !DO i=1,6
        !    J_r(i) = - J_r(12-i)
        !END DO
        !IF (messages) print*, '      FORCE J'
        !print*, J_r(1:12) END REMOVE
        ! store contribution of temporal derivative of vector potential in obsolete component of A
        A_r(8,:) = dt_A_r_dummy(:)
        ! TODO delete the following line
        !A_r(6,:) = L_r
        ! calculate radial partial diff (cylindrical system) of J_r in t_0,r_i
        IF (allocated(p_r)) DEALLOCATE(p_r)
        ALLOCATE(p_r(size(J_r)))
        CALL oneD_partial(spacestep,J_r,p_r,.false.)
        IF (messages) print*, '      DID PARTIAL DERIVATIVE'
        ! calculate j_r/r for t_0,r_i,
        ! with l'Hopital in 0, where von Neumann boundary is applied
        ! remind that node 6 is space 0
        IF (allocated(J_r_dividedby_r)) DEALLOCATE(J_r_dividedby_r)
        ALLOCATE(J_r_dividedby_r(size(J_r)))
        !$OMP PARALLEL SHARED(J_r_dividedby_r,J_r,p_r,spacestep) PRIVATE(i)
        !$OMP DO
        DO i=1,size(J_r)
            IF (i .ne. 6 .and. i .ne. size(J_r-6)) THEN
                J_r_dividedby_r(i) = J_r(i)/((real(i-6,kind=q)+0.)*spacestep)
            ELSE
                J_r_dividedby_r(i) = p_r(i)
            END IF
        END DO
        !$OMP END DO
        !$OMP END PARALLEL
        DO i = 1,6
            J_r_dividedby_r(size(J_r_dividedby_r)-6+i) = J_r_dividedby_r(size(J_r_dividedby_r)-6-i)
        END DO
        IF (messages) print*, '      DID RADIAL'
		! print table
        IF (messages) THEN
            print*,'node',' rho(t-1)',' drho',' drJ',' J/r'
            DO i=1,11
                print*,i,rho(i),timestep * (p_r(i) + J_r_dividedby_r(i) ),p_r(i),J_r_dividedby_r(i)
            END DO
        END IF
        ! future charge density by continuity equation,
        ! one step discretization
        ! HARDFIX counter oscillating solution
		! note: ([n+1]+[n-1]+2[n])/4 instead n 
		!       =>  try to cure instability by mean (motivated by von Neumann stability analysis)
        rho(2:size(rho)-1) = 0.25*(rho(1:size(rho)-2) + 2.*rho(2:size(rho)-1) + rho(3:size(rho))) &
                           & - timestep * (p_r + J_r_dividedby_r )
        rho(1) = rho(11)
        rho(size(rho)) = rho(size(rho)-11)
        IF (messages) print*, '      DID NEW RHO'
        ! fix to instable 0-node via boundary condition 0 = backward finite difference of 5th order accuracy
        !print*, rho(6), - (-300.*rho(5)+300.*rho(4)-200.*rho(3)+75.*rho(2)-12.*rho(1)) / 137.
        !rho(6) = - (-300.*rho(5)+300.*rho(4)-200.*rho(3)+75.*rho(2)-12.*rho(1)) / 137.
        ! END MAIN
    END SUBROUTINE
    ! RENORMALIZE CHARGE DENSITY
    ! integral charge
    FUNCTION integrate_rho(rho,spacestep,skin_depth,n_it)
        ! DEPENDENCIES
        ! ..
        ! VARIABLES
        IMPLICIT none
        ! input
        REAL(kind=q),              INTENT(IN)    :: spacestep          !! spacestep
        REAL(kind=q),              INTENT(IN)    :: skin_depth         !! skin depth
        REAL(kind=q), ALLOCATABLE, INTENT(IN)    :: rho(:)             !! charge density in (r_i)
        INTEGER                            :: n_it               !! integrate until this discrete space slice
        ! handling
        INTEGER                            :: i                  !! iteration
        REAL(kind=q)                             :: integral_charge    !! dummy
        ! self output
        REAL(kind=q)                             :: integrate_rho
        ! MAIN
        ! obtain simulated charge
        integral_charge = -spacestep**2*real(min(n_it,size(rho)-6),kind=q)*rho(min(n_it,size(rho)-6)) / 2.
        !$OMP PARALLEL SHARED(rho,n_it,spacestep) PRIVATE(i)
        !$OMP DO REDUCTION(+:integral_charge)
        DO i=6+1,min(n_it,size(rho)-6) - 1
            integral_charge = integral_charge + real(i-6,kind=q) * spacestep**2 * rho(i)
        END DO
        !$OMP END DO
        !$OMP END PARALLEL
        integrate_rho = integral_charge * 2. * pi * skin_depth
        ! END MAIN
    END FUNCTION
    ! brut
    SUBROUTINE renorm_rho(rho,total_Q_at_t,spacestep,skin_depth)
        ! DEPENDENCIES
        ! ..
        ! VARIABLES
        IMPLICIT none
        REAL(kind=q),              INTENT(IN)    :: total_Q_at_t       !! total charge
        REAL(kind=q),              INTENT(IN)    :: spacestep          !! spacestep
        REAL(kind=q),              INTENT(IN)    :: skin_depth         !! skin depth
        REAL(kind=q), ALLOCATABLE, INTENT(INOUT) :: rho(:)             !! charge density in (r_i)
        REAL(kind=q)                             :: integral_charge    !! integrated rho
        INTEGER                            :: i                  !! iteration
        ! MAIN
        ! obtain simulated charge
        integral_charge = integrate_rho(rho,spacestep,skin_depth,size(rho))
        ! renormalize with actual charge
        rho = rho * total_Q_at_t / integral_charge
        ! END MAIN
    END SUBROUTINE
    
END MODULE methods_physics

! #############################################################
! #############################################################
! MAIN: RUN PROGRAM
! #############################################################
PROGRAM chchd

    ! BIND MODULES
    USE methods_io          !! for input-output data management
    USE methods_system      !! for system functions
    USE methods_caretaker   !! for simulation house-keeping
    
    USE methods_math        !! advanced non-native math functions
    
    USE variables_input     !! for input relevant variables
    
    USE methods_physics     !! for physics equations
    
    ! VARIABLES
    ! simulation box
    REAL(kind=q)  :: timestep,spacestep            !! step sizes
    INTEGER :: M                             !! number of spatial gridpoints
    INTEGER :: N_CH                          !! discrete extend of focal spot
    ! physical parameters
    REAL(kind=q) :: focal_spot_area
    REAL(kind=q) :: skin_depth
    ! physical variables
    REAL(kind=q), ALLOCATABLE :: change_rho(:)  !! container for external changes to charge density
    REAL(kind=q), ALLOCATABLE :: rho(:)         !! container for charge density distribution in (space)
    REAL(kind=q), ALLOCATABLE :: A_r(:,:)       !! container for vector potential in (time,space)
    REAL(kind=q), ALLOCATABLE :: Phi(:,:)       !! container for electric potential in (time,space)
    REAL(kind=q), ALLOCATABLE :: J_r(:)         !! container for current density in (space)
    REAL(kind=q)              :: integral_Q     !! integaral Q within focal spot of laser
    ! iteration
    INTEGER :: f,steps,step,i,j
    ! helper
    REAL(kind=q), ALLOCATABLE :: variable1Dreal(:)
    REAL(kind=q) :: speed_of_light,nresistivity,npermeability,npermittivity
    ! io
    LOGICAL   :: windows_flag
    INTEGER   :: filenumber,filenumber_timestep,ppos,filenumber_t
    CHARACTER(len=1024) :: string_var,output_path,path_sep
    
    ! Type setup from methods_system
    CALL ask_selectedrealkind()

    ! INPUT
    ! Get the input filename if one was provided
    ! SUBROUTINE parse_commandline(OUT) from methods_io
    ! CHARACTER(:) ALLOCATABLE file_name_in from methods_io
    CALL parse_commandline(file_name_in)
    
    ! If we got a filename, continue, otherwise exit
    IF (allocated(file_name_in) .eqv. .false. ) THEN
        ! SUBROUTINE exit_chchd(IN) from methods_system
        CALL exit_chchd('No input file given!')
    END IF
    
    ! GIVE HELPER SCRIPT FOR EASY PLOTTING
    ! of saved files (col1 col2) in N rows
    IF (export_stepwise) THEN
        print*, '> PRODUCE 2D PLOTTER SCRIPTS'
        print*, '  CALL WITH: python plot_timestep.fpy path/to/file.dat'
        ! time VS quantity
        CALL write_pyplot("plot_timestep.fpy")
    END IF
    
    ! MAIN LOOP OVER INPUT FILES
    DO f=1,size(file_name_in)
        ! If we got a filename, try to open and read it
        ! SUBROUTINE read_inputfile(IN,OUT,OUT) from methods_io
        ! CHARACTER(:) ALLOCATABLE file_name_in from methods_io
        ! REAL ALLOCATABLE chocolat_meta from variables_input
        ! REAL ALLOCATABLE chocolat_scalar from variables_input
        CALL read_inputfile(file_name_in(f),chocolat_meta,chocolat_scalar)
        
        ! PREPARE OUTPUT
        ! create output directory from filename stub
        write(output_path,'(a)') trim(file_name_in(f))
        ppos = scan(trim(output_path),".", BACK= .true.)
        CALL create_outputdirectory(output_path(1:ppos-1))
        ! assemble
        INQUIRE(file='c:/windows',exist=windows_flag) 
        IF (windows_flag) THEN
            write(path_sep,'(a)') '\'
        ELSE
            write(path_sep,'(a)') '/'
        END IF
        
        CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                               &trim(path_sep) // "chargeinspot.dat",filenumber)
        CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                               &trim(path_sep) // "chargeinsim.dat",filenumber_t)                       
        ! write header
        write(filenumber,'(a)') "#time/fs#charge/nC"
        write(filenumber_t,'(a)') "#time/fs#charge/nC"
        
        ! renormalization
        chocolat_meta%dt      = chocolat_meta%dt * ps_to_s / T_over_nT
        chocolat_meta%d_targe = chocolat_meta%d_targe * um_to_m / R_over_nR
        chocolat_meta%d_laser = chocolat_meta%d_laser * um_to_m / R_over_nR
        speed_of_light        = speed_of_light_SI * (T_over_nT/R_over_nR)
        
        chocolat_scalar%Q     = chocolat_scalar%Q * nC_to_C / Q_over_nQ
        
        nresistivity = resistivity_SI(chocolat_meta%metal) / (M_over_nM * R_over_nR**3 / Q_over_nQ / T_over_nT)
        
        npermeability = vacuum_permeability_SI * Q_over_nQ**2 / (M_over_nM * R_over_nR)
        
        npermittivity = vacuum_permittivity_SI * R_over_nR**3 * M_over_nM / (Q_over_nQ * T_over_nT)**2
        
        print*, '> RESUME NORMALIZED PHYSICS'
        print*, '  * S-O-L      :: ',speed_of_light
        print*, '  * RESISTIV.  :: ',nresistivity
        print*, '  * PERMITTIV. :: ',npermittivity
		print*, '  * PERMEABIL. :: ',npermeability
        
        ! SIMULATION
        ! ELECTRIC- AND VECTOR-POTENTIAL WAVES w\ OHM & CONTINUITY
        ! LOOP   : 1) update source to actual rho
        !          2) calculate future electric potential
        !          2) calculate furure vector potential
        !          3) calculate actual current density
        !          4) calculate future rho
        ! REQUIRES
        ! Set up simulation parameters and space
        ! REAL conductivity from variables_input
        ! REAL vacuum_permittivity_SI from variables_input
        ! REAL speed_of_light_SI from variables_input
        ! REAL speed_of_light_SI from variables_input
        ! REAL chocolat_meta from variables_input
        ! REAL s_to_ps from variables_input
        ! REAL m_to_mm from variables_input
        ! define grid parameters
        print*, '> RESUME PARAMETERS'
        timestep  = chocolat_meta%dt * real(chocolat_meta%diag_s,kind=q)
        IF (expandSteps_factor .gt. 0) timestep = timestep / real(expandSteps_factor+1,kind=q)
        spacestep = timestep * speed_of_light ! respect Courant–Friedrichs–Lewy condition with C = 0.1 - TODO beware of 8th order accuracy
        IF (decrese_spacestep .eqv. .false.) spacestep = spacestep * real(expandSteps_factor+1,kind=q)
		IF (spatial_resolution_factor .gt. 0 .and. spatial_resolution_factor .ne. 1.) spacestep = spacestep / spatial_resolution_factor
        print*, '  * TIMESTEP   :: ',timestep
        print*, '  * SPACESTEP  :: ',spacestep
        print*, '  * COURANT #  :: ',timestep/spacestep*speed_of_light
        ! prepare container for externally applied changes to the charge density
        print*, '  * T-RADIUS   :: ',chocolat_meta%d_targe/2.
        M = int(chocolat_meta%d_targe/2./spacestep)
        ! add margin to avoid calculation on boundaries
        M = M + 6 + 6
        print*, '  * GRIDPOINTS :: ',M
        IF(allocated(change_rho)) DEALLOCATE(change_rho)
        ALLOCATE(change_rho(M))
        change_rho = 0.
        ! prepare containers for potentials and densities
        IF(allocated(rho)) DEALLOCATE(rho)
        ALLOCATE(rho(M))
        rho = 0.
        IF(allocated(J_r)) DEALLOCATE(J_r)
        ALLOCATE(J_r(M))
        J_r = 0.
        IF(allocated(Phi)) DEALLOCATE(Phi)
        ALLOCATE(Phi(8,M))
        Phi = 0.
        IF(allocated(A_r)) DEALLOCATE(A_r)
        ALLOCATE(A_r(8,M))
        A_r = 0.
        ! find discrete extend of focal spot area
        N_CH = int(chocolat_meta%d_laser/2./spacestep)
        print*, '  * SPOT CELLS :: ',N_CH
        ! calculate focal spot area
        focal_spot_area = (chocolat_meta%d_laser/2.)**2 * pi
        print*, '  * SPOT AREA  :: ',focal_spot_area
        ! estimate skin depth
        skin_depth = sqrt(timescale*resistivity_SI(chocolat_meta%metal)/pi/vacuum_permeability_SI) / R_over_nR
        print*, '  * SKINDEPTH  :: ',skin_depth
        ! get steps
        print*, '> EXPAND SIMULATION'
        IF (expandSteps_factor .lt. 0) CALL exit_chchd('The step diminuator must be positive.')
        ! steps = (size(chocolat_scalar%Q)-1) * (expandSteps_factor+1)
        CALL interpolate1Dreal(chocolat_scalar%Q,expandSteps_factor,'newton')
        steps = size(chocolat_scalar%Q)
        ! controll
        !CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
        !                       &trim(path_sep) // "expanded.dat",filenumber_timestep)
        !DO step=1,steps
        !    write(filenumber_timestep,*) real(step,kind=q)*timestep, chocolat_scalar%Q(step)
        !END DO
        !CALL close_file(filenumber_timestep)
        ! evolve
        print*, '> ENTER EVOLUTION'
        print*, '  * STEPS      :: ', steps
        DO step=1,steps
            IF (messages) print*, '  * ENTER STEP',step
            ! load discharge from chocolat
            IF (step .gt. 1 .and. step .lt. 42) change_rho = rho_gausz(spacestep, &
                                                    &(chocolat_scalar%Q(step) - &
                                                    & chocolat_scalar%Q(step-1)), &
                                                    &chocolat_meta%d_laser, &
                                                    &skin_depth, &
                                                    &size(change_rho))
            IF (step .eq. 1 ) change_rho = rho_gausz(spacestep, &
                                                    &chocolat_scalar%Q(step), &
                                                    &chocolat_meta%d_laser, &
                                                    &skin_depth, &
                                                    &size(change_rho))
            IF (messages) print*, '    DISCHARGED'
            ! update potentials and step up charge density
            CALL timestep_potential(nresistivity, npermeability, npermittivity,&
                                   & 1./speed_of_light**2, &
                                   &timestep, &
                                   &spacestep, &
                                   &Phi,rho,change_rho,A_r,J_r)
            IF (messages) print*, '    UPDATED'
            ! renormalization and multi-pass smoothing
            CALL renorm_rho(rho,chocolat_scalar%Q(step),spacestep,skin_depth)
            ! OPTIONAL
            DO j=0,2
                CALL GaussianKernel(rho,6-1)
                DO i=1,6
                    ! inner
                    rho(6-(i-1)) = rho(6+(i-1))
                    ! outer
                    rho(size(rho)-6+i) = rho(size(rho)-6-i)
                END DO
            ENDDO
            !CALL renorm_rho(rho,chocolat_scalar%Q(step)* nC_to_C,spacestep,skin_depth)
            ! END OPTIONAL
            IF (messages) print*, '    RENORMALIZED, MAXVAL =, MEANVAL ='
            IF (messages) print*, maxval(rho), sum(rho)/(max(1,size(rho)))
            ! spatially resolved step data extraction
            IF (export_stepwise .and. MOD(step-1,expandSteps_factor+1) .eq. 0) THEN
                IF (MOD((step-1)/(expandSteps_factor+1),export_stepwise_ith) .eq. 0) THEN
                    ! write out values of THIS timestep
                    write(string_var,'(I0)') (step-1)/(expandSteps_factor+1) + 1
                    ! WRITE CURRENT DENSITY
                    CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                                           &trim(path_sep) // "jofr_"//trim(string_var)//".dat",filenumber_timestep)
                    write(filenumber_timestep,'(a)') "#radial/mm#currentdensity/SU"
                    DO i=1,size(J_r)
                        write(filenumber_timestep,*) real(i-6,kind=q) * spacestep * R_over_nR * m_to_mm, J_r(i)
                    END DO
                    CALL close_file(filenumber_timestep)
                    ! WRITE RADIAL DERIVATIVE OF ELECTRIC POTENTIAL
                    CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                                           &trim(path_sep) // "drphiofr_"//trim(string_var)//".dat",filenumber_timestep)
                    write(filenumber_timestep,'(a)') "#radial/mm#dr_potential/SU"
                    DO i=1,size(J_r)
                        write(filenumber_timestep,*) real(i-6,kind=q) * spacestep * R_over_nR * m_to_mm, Phi(8,i)
                    END DO
                    CALL close_file(filenumber_timestep)
                    ! WRITE TEMPORAL DERIVATIVE OF VECTOR POTENTIAL
                    CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                                           &trim(path_sep) // "dtAofr_"//trim(string_var)//".dat",filenumber_timestep)
                    write(filenumber_timestep,'(a)') "#radial/mm#dt_vectorpotential/SU"
                    DO i=1,size(J_r)
                        write(filenumber_timestep,*) real(i-6,kind=q) * spacestep * R_over_nR * m_to_mm, A_r(8,i)
                    END DO
                    CALL close_file(filenumber_timestep)
                    ! write out values of NEXT timestep
                    write(string_var,'(I0)') (step-1)/(expandSteps_factor+1) + 2
                    ! WRITE CHARGE DENSITY
                    CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                                           &trim(path_sep) // "rhoofr_"//trim(string_var)//".dat",filenumber_timestep)
                    write(filenumber_timestep,'(a)') "#radial/mm#chargedensity/SU"
                    DO i=1,size(rho)
                        write(filenumber_timestep,*) real(i-6,kind=q) * spacestep * R_over_nR * m_to_mm, rho(i)
                    END DO
                    CALL close_file(filenumber_timestep)
                    ! WRITE ELECTRIC POTENTIAL
                    CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                                           &trim(path_sep) // "phiofr_"//trim(string_var)//".dat",filenumber_timestep)
                    write(filenumber_timestep,'(a)') "#radial/mm#potential/SU"
                    DO i=1,size(J_r)
                        write(filenumber_timestep,*) real(i-6,kind=q) * spacestep * R_over_nR * m_to_mm, Phi(1,i)
                    END DO
                    CALL close_file(filenumber_timestep)
                    ! WRITE VECTOR POTENTIAL
                    CALL prepare_outputfile(trim(output_path(1:ppos-1)) // &
                                           &trim(path_sep) // "Aofr_"//trim(string_var)//".dat",filenumber_timestep)
                    write(filenumber_timestep,'(a)') "#radial/mm#vectorpotential/SU"
                    DO i=1,size(J_r)
                        write(filenumber_timestep,*) real(i-6,kind=q) * spacestep * R_over_nR * m_to_mm, A_r(1,i)
                    END DO
                    CALL close_file(filenumber_timestep)
                END IF
            END IF
            ! extraction of temporal summary of step
			IF (messages) print*, '    CLOSE UP ON TIMESTEP : / [fs]'
			IF (messages) print*, '    ',real(step,kind=q)*timestep*T_over_nT*s_to_fs
            integral_Q = integrate_rho(rho,spacestep,skin_depth,N_CH)
            write(filenumber,*) real(step,kind=q)*timestep*T_over_nT*s_to_fs, integral_Q * Q_over_nQ &
                                                     & * C_to_nC
            write(filenumber_t,*) real(step,kind=q)*timestep*T_over_nT*s_to_fs, chocolat_scalar%Q(step) * Q_over_nQ &
                                                     & * C_to_nC
            ! emergency exit
            IF (integral_Q .ne. integral_Q) THEN
                print*, '    CATCH NaN'
                print*, rho(1:N_CH)
                EXIT
            ENDIF
			!MAYBE scheduled exit
			!IF (step .gt. 1702) EXIT
        END DO
        print*, '  END EVOLUTION'
        CALL close_file(filenumber)
        CALL close_file(filenumber_t)
        
        ! OUT
        print*, "> max(OUT) / [SU]"
        print*, maxval(rho)
        
    END DO

! #############################################################
! FOOTER: HELPER FUNCTIONS AND EXIT PROGRAM
! #############################################################
    ! Exit gracefully
    ! SUBROUTINE exit_chchd(IN) from methods_system
    CALL exit_chchd('Exitcute!')
END PROGRAM chchd