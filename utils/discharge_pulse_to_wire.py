# -*- coding: utf-8 -*-
"""
This add-in can be used to calculate estimates for pulse width and decay
parameter of discharge pulses. The modelling can be found in Ehret2021.
The basis of estimates are total target discharge and discharge time from
ChoCoLaT-2.

The group velocity must be given, as well as target parameters.

"""
# =============================================================================
# VARIABLES
# =============================================================================
# USER INPUT
# discharge time in [ps]
tau_0 = 6.1
# uncertainty of discharge time [ps]
d_tau = 0.5
# total target discharge in [nC]
Q_0 = 219.
# uncertainty of total target discharge in [nC]
d_Q = 21.
# group velocity in fractions of speed of light
c_g = 1.
# wire diameter in [mm]
d_w = 0.2
# target disk radius in [mm]
R_d = 1.1
# laser focal spot radius in [mm]
r_L = 0.0035
# initial target temperature in [K]
T_0 = 300.
# discharge pulse model
# .. schleifer = case for recepient on larger target
dischargemodel = 'schleifer'

# =============================================================================
# PARAMETERS
# =============================================================================
# ITERATIONS
# start vaue for kappa iteration (Newton's method)
kappa_start = 1.
# tolerance
new_tol = 1.48e-8
# max iteration
new_maxiter = 500

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL
import os
import sys
import time

import numpy as np

import scipy.optimize

# ==============================================================================
# CONSTANTS
# ==============================================================================
# PHYSICS
speed_of_light = 299792458            # !! speed of light in vacuum
avogadro = 6.02214076e23              # !! Avogadro constant
m_u = 1.660539066e-27                 # !! atomic mass unit

# UNITS
m_to_mm = 1.e3
s_to_ps = 1.e12
C_to_nC = 1.e9

# MATERIALS
materials = {}

materials['Wo'] = {\
    'name':'Tungsten',\
    'resistivity_SI': 52.8e-9,\
    'massnumber': 183.84,\
    'massdensity_SI': 19.3e3,\
    'meltingLatentHeat_Jmol-1': 53.31e3,\
    'heatcapacity_Jmol-1K-1': 24.27,\
    'meltingTemperature_K': 3695.\
}

materials['Cu'] = {\
    'name':'Copper',\
    'resistivity_SI': 16.78e-9,\
    'massnumber': 63.546,\
    'massdensity_SI': 8.96e3,\
    'meltingLatentHeat_Jmol-1': 13.26e3,\
    'heatcapacity_Jmol-1K-1': 24.44,\
    'meltingTemperature_K': 1357.77\
}

materials['Mo'] = {\
    'name':'Molybdenum',\
    'resistivity_SI': 53.4e-9,\
    'massnumber': 95.95,\
    'massdensity_SI': 10.28e3,\
    'meltingLatentHeat_Jmol-1': 37.48e3,\
    'heatcapacity_Jmol-1K-1': 24.06,\
    'meltingTemperature_K': 2896.\
}


materials['StainlessSteel'] = {\
    'name':'Stainless Steel',\
    'resistivity_SI': np.array([161.1,749.6])*1e-9,\
    'massnumber': 0.89*55.845 + 0.11*51.9961,\
    'massdensity_SI': 8.03e3,\
    'meltingLatentHeat_Jmol-1': 0.89*18.8e3+0.11*21.e3,\
    'heatcapacity_Jmol-1K-1': 0.89*25.1+0.11*23.35,\
    'meltingTemperature_K': 2507.\
}

# ==============================================================================
# FUNCTIONS
# ==============================================================================
# DISCHARGE PULSE AS LINEAR CHARGE DISTRIBUTION
def lam(t,v_g,kappa,sigma,Q_0):
    factor = Q_0 * kappa / (kappa+np.exp(1.)-1.) / sigma
    expression = np.where(v_g * t < sigma, np.where(v_g * t < 0,0.,(v_g*t/sigma)**(kappa-1.)*np.exp(1.-(v_g*t/sigma)**kappa)),np.exp(1.-(v_g*t/sigma)))
    return factor * expression

# DISCHARGE PULSE MAXIMUM
def lam_max(d_w,R_d,r_L,tau_0,v_g,Q_0):
    return d_w / (np.pi * (R_d*2.*r_L+R_d*2.*v_g*tau_0-(r_L+v_g*tau_0)**2)) * Q_0 * (r_L+v_g*tau_0)/(v_g*tau_0) * (1.-np.exp(-v_g*tau_0/(r_L+v_g*tau_0)))

# SIGMA: BOUNDARY CONDITION FROM RISE TIME
def sigma(kappa,v_g,tau_0):
    return v_g * tau_0 / (1.-1./kappa)**(1./kappa)

# ZERO CONDITION FROM CONTINIOUS DISCHARGE MODEL
def B_0(kappa,lam_m,tau_0,v_g,Q_0):
    return lam_m - Q_0/(v_g*tau_0) * (kappa-1.)/(kappa-1.+np.exp(1.)) * np.exp(1./kappa)

# ==============================================================================
# MAIN
# ==============================================================================
# SELF RUN
if __name__ == "__main__":
    """
    If this script is executed as master, the main pulse parameters are calculated
    and the discharge pulse is plotted.
    """
    # calculate group velocity in [mm/ps]
    v_g = speed_of_light * c_g * m_to_mm/s_to_ps
    # decide for fraction of discharge to take into account
    if dischargemodel == 'schleifer':
        Q_e = Q_0 * d_w / (2. * np.pi * R_d)
    else:
        Q_e = Q_0
    # calculate the sicharge pulse maximum [nC/mm]
    lam_m = lam_max(d_w,R_d,r_L,tau_0,v_g,Q_e)
    print("PEAK EQUIVALENT LINEAR CHARGE DENSITY [nC/mm]")
    print(lam_m)
    # give feedback on current peak
    I_m = lam_m * v_g
    print("PEAK EQUIVALENT CURRENT [kA]")
    print(I_m)
    # Newton's method
    kappa_0 = scipy.optimize.newton(B_0,kappa_start,fprime=None, args=(lam_m,tau_0,v_g,Q_e), tol=new_tol, maxiter=new_maxiter, fprime2=None)
    # screen output
    print("KAPPA [#]:")
    print(kappa_0)
    # get sigma
    sigma_0 = sigma(kappa_0,v_g,tau_0)
    # screen output
    print("SIGMA [mm]:")
    print(sigma_0)
    print("SIGMA [ps]:")
    print(sigma_0/v_g)
    # plot
    
    # save
    
    # infos about wire (E_melting > E_ohm - E_heating)
    pulse_factor = lam_m**2 * v_g * sigma_0 # in ps nC
    print("PULSE FACTOR [nC2ps-1]")
    print(pulse_factor)
    for material in materials.keys():
        print("PROPOSE "+materials[material]['name'])
        material_factor = materials[material]['resistivity_SI'] * materials[material]['massnumber'] * m_u / materials[material]['massdensity_SI'] / (materials[material]['meltingLatentHeat_Jmol-1'] + (materials[material]['meltingTemperature_K'] - T_0)*materials[material]['heatcapacity_Jmol-1K-1']) * avogadro # in SI
        print("MATERIAL FACTOR [mm4ps1nC-2]")
        material_factor = material_factor * m_to_mm**4 * s_to_ps / C_to_nC**2 # convert to mm ps nC
        print(material_factor)
        minimum_surface = np.sqrt(pulse_factor * material_factor)
        print("MINIMUM SURFACE BEFORE MELTING [mm2]")
        print(minimum_surface)
        print("MINIMUM RADIUS OF EQUIVALENT CYLINDER BEFORE MELTING [mm]")
        minimum_eq_radius = np.sqrt(minimum_surface/np.pi)
        print(minimum_eq_radius)
    
    