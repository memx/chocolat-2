!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%% ChoCoLaT vII.1.75 %%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%% Laser Target Charg'omatic Code %%%% version II.1.75 %%%%
!%%%%%%%%%%%%%% for laser-mater interaction %%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%% Developped at ENS during years 2016-1018 %%%%%%%%%%
!%%%%%%%%%%%%% by Alexandre Poyé %%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%% alexandre.poye@gmail.com %%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%% Please contact for further developments %%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%% or updates %%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!Program goal: solving equations for the evolution of a hot electron cloud in a thick target and calculating the amount of electron ejected.
!Tree physical phenomena are included : hot electron cooling by target collision, hot electron ejection, hot electron creation by laser.

!program input argument : I4 interger used as a file number.
!input data from input file: 'input_****.dat' containing :
! % metal   =  I4                    (1:carbone 2:aluminum 3:cuivre 4:tungstene 5:tantale 6:plomb)
! % N_iks   =  I6                    (number of point for the distribution function discretisation in mec² )
! % dt      =  E11.4                 (time step for initial value solver, in ps)
! % Nb_Th     =  I4                  (energy maximum of the distribution function as Nb_th * Th (eq:B1) )
! % diag_s    =  I4                  (1 save every diag_s iterations for the scalar diagnostic. 0 is setting the diagnostic off )
! % diag_f    =  I4                  (1 save every diag_h iterations for the distribution function diagnostic. 0 is setting the diagnostic off )
! % t_laser =  E11.4                 (duration of the laser pulse, in ps)
! % E_laser =  E11.4                 (energy of the laser pulse, in J)                    
! % C_abs   =  E11.4                 (absorbtion coefficent of the laser pulse, from 0 to 1)                    
! % w_laser =  E11.4                 (wavelength of the laser pulse, in µm)                  
! % d_laser =  E11.4                 (diameter of the laser spot, in µm)                    
! % d_targe =  E11.4                 (diameter of the target, in µm)   
! % e_targe =  E11.4                 (thickness of the target, in µm) 

! MEmx TODO the conductivity is only affecting the spread of the potential on the target - which should be more a matter
! of ionization AND the current is not Alfen limited if there is no conductivity - is that ok?

! MEmx made absorption automatic if negative


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%% parameters list module %%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
module parameters
implicit none
!electron cloud variables
real*8 :: J        ! total current ejected in kA (eq 15a)
real*8 :: N        ! charge in the hot cloud in nC (eq 10a)
real*8 :: Q        ! charge ejected from the target in nC (eq 15b)
real*8 :: heat_matter  ! energy transferred to ambient matter by colissions in J
real*8 :: heat_ejected ! energy ejected in form of electrons in J
real*8 :: heat_cloud   ! energy stored in electron distribution in J
real*8 :: vol      ! average volume of the hot electron cloud in µm^3 (eq 10b)
real*8 :: RLD      ! average ratio between cloud radius and Debye length (used in eq 17)
real*8 :: ne       ! average electron density in the cloud in nC/µm^3 (eq 10a and eq 10b)
real*8 :: LambdaD  ! Debye length in µm (used in eq 17, 19, 20a and 20b)
real*8 :: phi      ! potential to normalised the temperature ( eq 17, 19, 20a, 20b, 21a, 21b and 22)
real*8 :: eLD      ! ratio between tharget thickness and Debye length (used in eq 19, 20a and 20b)
real*8 :: T_hot_e  ! hot electron cloud in mec² (eq 3)
real*8 :: Rav     ! mean cloud radius in µm (eq 19)
!potential barrier variables
real*8 :: phi_th ! Normalized thermal potential (eq 17 or 19)
real*8 :: phi_E  ! Normalized electrostatic potential (eq 22)
real*8, dimension(:),allocatable :: J_time,R_time,Time 
!J,R,time saving tables for electrostatic potential calculation (eq 22)
logical :: phi_E_plugged = .true.

!time variables
real*8 :: dt     ! time step in ps.
real*8 :: t      ! time in ps.
real*8 :: t_max  ! maximal time for J_time, R_time and Time array boundary, in ps.
real*8 :: d_T    ! time step for tee calculation, in ps.

!laser variables
real*8 :: w_laser   ! laser wavelength in µm.
real*8 :: r_laser   ! laser spot radius in µm.
real*8 :: t_laser   ! laser pulse duration in ps.
real*8 :: E_laser   ! laser energy in J.
real*8 :: C_abs     ! absorbtion coefficient.
real*8 :: P_laser   ! laser power in TODO
real*8 :: I_laser   ! laser intensity in TODO  
real*8 :: a0        ! normalised laser intensity (eq 3)
real*8 :: d_laser   ! laser spot diameter in µm.
real*8, dimension(:), allocatable :: f_0 ! distribution function of the electron generated by the laser in nC/mec² (eq 2).
real*8 :: Em0       ! mean energy of f_0 in mec²


!distribution function arrays
real*8, dimension(:), allocatable :: iks_table      ! TODO
real*8 ::  d_iks  ! energy step for distribution function in mec²
integer :: i_iks  ! counter for energy distribution and energy arrays.
integer :: N_iks  ! Number of of points for the distribution funktions
integer :: Nb_Th  ! energy maximum of the distribution function in units of T_hot_e
real*8, dimension(:), allocatable :: f_table        ! distribution function of the number of hot electron in the target in nC/mec² (eq 1).
real*8, dimension(:), allocatable :: mheating_table ! distribution that catches losses to material by collissions (tee) in nC/mec²
real*8, dimension(:), allocatable :: gfr_table      ! distribution function of the number of hot electron escaping the target from front side in nC/mec² (eq 1).
real*8, dimension(:), allocatable :: gre_table      ! distribution function of the number of hot electron escaping the target from rear side in nC/mec² (eq 1).

! table of variable for each \epsilon_0 for tee calculation (eq 6).
real*8, dimension(:), allocatable :: v_table        ! speed of hot electron in the target in µm/ps (eq 7b).
real*8, dimension(:), allocatable :: e_table        ! energy of hot electron in the target in (mec²) 
real*8, dimension(:), allocatable :: rmax_table     ! maximal range of hot electron in the target in µm (eq 4)
real*8, dimension(:), allocatable :: r_table        ! range of hot electron in the target in µm (eq 8) (radial)
real*8, dimension(:), allocatable :: z_table        ! range of hot electron in the target in µm (eq 8) (axial)
real*8, dimension(:), allocatable :: sfr_table        ! section of the cloud in µm² (eq 9a)
real*8, dimension(:), allocatable :: vol_table      ! volume of the cloud in µm³ (eq 9b)
real*8, dimension(:), allocatable :: xE_table       ! energetic distance in µm (eq 12a)
real*8, dimension(:), allocatable :: C_slow
!real*8, dimension(:), allocatable :: xD_table       ! energetic distance in µm (eq 12a)
!real*8, dimension(:), allocatable :: rB_table       ! back scattering radius in µm (eq 12a)
real*8, dimension(:), allocatable :: thetafr_table  ! scattering angle from the front side (eq 12b)
integer :: i_phi                                    ! energy index for the potential at front side.
! rear side value for 
real*8, dimension(:), allocatable :: thetare_table  ! scattering angle from the rear side (eq 14)
real*8, dimension(:), allocatable :: sre_table  ! scattering angle from the rear side (eq 14)

! table of variable for each \epsilon_0 for tee calculation (eq 6).
real*8, dimension(:), allocatable :: e1_table       ! energy of hot electron in the target in (mec²) (eq 7b).
real*8, dimension(:), allocatable :: v1_table       ! speed of hot electron in the target in µm/ps (eq 7b).
real*8, dimension(:), allocatable :: r1_table       ! range of hot electron in the target in µm (eq 8)
real*8, dimension(:), allocatable :: tee_table      ! cooling time of electron in ps (eq 6)


real*8 :: Em ! TODO
 
!target variables
integer :: metal      ! kind of metal (1:carbone 2:aluminum 3:cuivre 4:tungstene 5:tantale 6:plomb)
real*8 :: Z_target    ! Atomic number
real*8 :: rho_target  ! target density g/cm³
real*8 :: A_target    ! Masse number
real*8 :: gg          ! coeficient for electron-ion scattering (eq 12).
real*8 :: cond        ! 0 for insulating material, 1 for conductive one.
real*8 :: d_targe     ! diameter of the target in µm.
real*8 :: e_targe     ! thickness of the target in µm.


! program management variables
integer :: counter                ! counter for the number of iterations
integer :: counter_diag           ! counter for the number of iterations between each scalar diag
integer :: counter_diag_heavy     ! counter for the number of iterations between each distrib function diag
integer :: iteration_diag         ! number of iterations spacing each scalar diag
integer :: iteration_diag_heavy   ! number of iterations spacing each distrib function diag
integer :: i



real*8 ::  pi= 3.1415926535897932384626433832795028841971693993751
logical :: getout                 ! conditions for the main while loop.  
real*8 :: cpu_T1, cpu_T2          ! chronometer time for the execution.
integer*4 :: num_arg,numbfile     ! String for file management.
character(len=22),dimension(:),allocatable :: arg_string     ! String for argument management.
character(len=4) :: char_a     ! String for file management.
character(len=22) :: N_iks_string ! String for distrib diagnostic.

end module parameters
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%% main programme %%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
program charge_target
use parameters
implicit none


!run chronometer start
call CPU_TIME(cpu_T1) 

!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%% INITIALISATION %%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%% default parameters %%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

!1: carbone
!2: aluminum
!3: cuivre
!4: tungstene
!5: tantale
!6: plomb
metal = 5
d_targe = 10000 !in um
e_targe = 30 !in um 

! Resolution parameters
dt = 0.001    ! time evolution resolution
N_iks = 2000  ! number of point for energy arrays
t_max = 3000. ! maximal time for simulation.
iteration_diag = 10
iteration_diag_heavy = 0
!beware of the effetive CFL dt > d_iks.


! laser parameter
t_laser = 0.1 ! in ps
E_laser = 5.0 ! in J
C_abs   = 0.4 
w_laser = 0.8! in um
d_laser = 12 ! in um
Nb_Th = 20


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%% read input file %%%%%%%%%%%%%%%
!%%%%%%%%%% create output file %%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
call files_manager ! read input files, create output files.

 !argument can be added to the launch command. They replace the input file arguments.
num_arg=command_argument_count() !count arguments
allocate(arg_string(1:num_arg))
if(num_arg > 1)then
  do i = 2,num_arg
    call getarg(i,arg_string(i)) ! read arguments
  enddo
  read (arg_string(2),"(E11.4)") e_targe ! distribute arguments in correspondant variable
  read (arg_string(3),"(E11.4)") E_laser 
  read (arg_string(4),"(E11.4)") t_laser 
  read (arg_string(5),"(I1)")    metal  
endif

call create_output_file(numbfile)
write(N_iks_string,'(I8)') N_iks ! used for the output write.
call metal_choice  ! fill material parameters with proper values.



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! calculation of initial temperature due to beg's law.
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
r_laser = d_laser/2./sqrt(log(2.))
t_laser = t_laser*sqrt(pi)/ (2*sqrt(log(2.)))
P_laser = E_laser/t_laser
I_laser = P_laser  / ( pi* r_laser**2)

! modify absorption with Yu1999 & Ken1998 if negative
if (C_abs < 0.) then
    C_abs = MIN(1.2*(I_laser)**(0.75),0.5)
    PRINT*,"C_abs to ",C_abs
endif

a0 = 0.853*w_laser*10*sqrt(I_laser)

if (a0 > 0.03 ) then
  T_hot_e = max(0.47*a0**(2./3.), sqrt(1+a0**2)-1)
else 
  T_hot_e = 3*a0**(4./3.)
endif

if(iteration_diag .NE. 0)then
  write(102, '(A11,E11.4)') '% T_hot_e = ', T_hot_e*511
  write(102, '(A11,E11.4)') '% C_abs   = ', C_abs
endif
if(iteration_diag_heavy .NE. 0)then
  write(104, '(A11,E11.4)') '% T_hot_e = ', T_hot_e*511
  write(105, '(A11,E11.4)') '% T_hot_e = ', T_hot_e*511
  write(106, '(A11,E11.4)') '% T_hot_e = ', T_hot_e*511
  write(107, '(A11,E11.4)') '% T_hot_e = ', T_hot_e*511
  write(104, '(A11,E11.4)') '% C_abs   = ', C_abs
  write(105, '(A11,E11.4)') '% C_abs   = ', C_abs
  write(106, '(A11,E11.4)') '% C_abs   = ', C_abs
  write(107, '(A11,E11.4)') '% C_abs   = ', C_abs
endif



!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%% initialisation of ChoCoLaT2 %%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

! energy resolution (eq B1)
d_iks = T_hot_e*Nb_Th/N_iks

 
!allocating f_distrib tables:
allocate(iks_table(1:N_iks),f_table(1:N_iks),mheating_table(1:N_iks),rmax_table(1:N_iks),f_0(1:N_iks), &
         r_table(1:N_iks), r1_table(1:N_iks), vol_table(1:N_iks), &
         e_table(1:N_iks), v_table(1:N_iks),e1_table(1:N_iks), v1_table(1:N_iks), &
         xE_table(1:N_iks), tee_table(1:N_iks), z_table(1:N_iks), C_slow(1:N_iks),&
         sfr_table(1:N_iks), sre_table(1:N_iks), gfr_table(1:N_iks), gre_table(1:N_iks),&
         thetafr_table(1:N_iks),thetare_table(1:N_iks))
! setting all to zero.
thetafr_table = 0.     
thetare_table = 0.
iks_table = 0.
f_table = 0.
e_table = 0.
v_table = 0.
e1_table = 0.
v1_table = 0.
r1_table = 0.
r_table = 0.
rmax_table = 0.
f_0 = 0.
gfr_table = 0.
gre_table = 0.
r_table = 0.
sfr_table = 0.
sre_table = 0.
vol_table = 0.
z_table = 0.
C_slow = 0.
! electron/ion collision rate.
gg = 0.187 * Z_target**(2./3.)

! filling tables for each epsilon_0.
!$OMP PARALLEL
!$OMP DO
do i_iks = 1,N_iks
  iks_table(i_iks) = 1+i_iks*d_iks
  !diffusion radius distribution function
  rmax_table(i_iks)= 2.76*10**3 *A_target *(0.511*(iks_table(i_iks)- 1 ))**(5./3.)*&
    Z_target**(-8./9.)*(1 + 1.957*(0.511*(iks_table(i_iks)- 1 )))**(-5./3.) *&
    (1 + 1*(0.511*(iks_table(i_iks)- 1 )+ 10**6*(0.511*(iks_table(i_iks)- 1 ))**4))**(1./8.) / rho_target  ! (eq 4)
    
  v_table(i_iks) = 300*sqrt(iks_table(i_iks)**2.-1)/iks_table(i_iks)  ! (eq 7b)

  e_table(i_iks) = (iks_table(i_iks)-1) 
  !v_table(i_iks) = 300*sqrt((5./8.*e_table(i_iks)+1)**2.-1)/(5./8.*e_table(i_iks)+1) ! (eq 7b) with mean energie 5/8.. poubelle.
    
  xE_table(i_iks)= rmax_table(i_iks)*(1+2.*gg-0.21*gg**2.)/(2.*(1+gg)**2.)      ! (eq 12a)
  
  C_slow(i_iks) = (rmax_table(i_iks) - min(xE_table(i_iks),e_targe))/rmax_table(i_iks)
  
!  rB_table(i_iks)= rmax_table(i_iks)*1.1*gg/(1+gg)      ! (eq 12a)
!  xD_table(i_iks)= rmax_table(i_iks)/(1+gg)             ! (eq 12a)

!  thetafr_table(i_iks) = atan( rB_table(i_iks) / min(xE_table(i_iks),e_targe) ) ! (eq 12b)
!  if(rmax_table(i_iks) < e_targe)then
!    thetare_table(i_iks) = 0.     ! (eq 14a)
!  elseif(rmax_table(i_iks) - xD_table(i_iks) > e_targe)then
!    thetare_table(i_iks) = pi/2. ! (eq 14c)
!  else
!    thetare_table(i_iks) = acos( (e_targe + xD_table(i_iks) - rmax_table(i_iks)) / xD_table(i_iks) ) ! (eq 14b)
!  endif
!  f_table(i_iks) =  iks_table(i_iks) * sqrt(iks_table(i_iks)**2-1) * exp(-(iks_table(i_iks)-1)/T_hot_e) MJ to check !
  
  f_0(i_iks) = exp(-(iks_table(i_iks)-1)/T_hot_e)  ! (eq 2) classical Maxwell-Boltzmann
enddo
!$OMP END DO
!$OMP END PARALLEL
  f_0 =  f_0 / (sum(f_0)*d_iks)             ! normalizing f_0
  Em0 = sum((iks_table-1)*f_0)*d_iks        ! is total energy, calculating the average energy by division with N (equal to T also !)


! calulation of t_ee (eq 6): energy loss calculation with time propagation and range reaching.
do i_iks = 1,N_iks
  t=0
  d_T = rmax_table(i_iks)/v_table(i_iks)/ 1000
  getout = .false.
  do while (getout .eqv. .false.)
    t=t+d_T
    e1_table(i_iks) = e_table(i_iks)*(1-r1_table(i_iks)/rmax_table(i_iks))**(3./5.)
    v1_table(i_iks) = 300.*sqrt((e1_table(i_iks)+1)**2.-1)/(e1_table(i_iks)+1)
    r1_table(i_iks) = r1_table(i_iks)+d_T*v1_table(i_iks)
    if(r1_table(i_iks) > 0.999*rmax_table(i_iks))then
      tee_table(i_iks) = t
      getout = .true.
    endif
  enddo
enddo


! writting parameters calulated.
if(iteration_diag .NE. 0)then
  write(102, '(A11,E11.4)') '% N_tot   = ', (10**3/0.511) * C_abs * E_laser/Em0
endif
if(iteration_diag_heavy .NE. 0)then
  write(104, '(A11,E11.4)') '% N_tot   = ', (10**3/0.511) * C_abs * E_laser/Em0
  write(105, '(A11,E11.4)') '% N_tot   = ', (10**3/0.511) * C_abs * E_laser/Em0
  write(106, '(A11,E11.4)') '% N_tot   = ', (10**3/0.511) * C_abs * E_laser/Em0
  write(107, '(A11,E11.4)') '% N_tot   = ', (10**3/0.511) * C_abs * E_laser/Em0
endif
if(iteration_diag .NE. 0)then
  write(102, '(A11,E11.4)') '% t_life  = ', sum(tee_table*f_0)*d_iks
endif
if(iteration_diag_heavy .NE. 0)then
  write(104, '(A11,E11.4)') '% t_life  = ', sum(tee_table*f_0)*d_iks
  write(105, '(A11,E11.4)') '% t_life  = ', sum(tee_table*f_0)*d_iks
  write(106, '(A11,E11.4)') '% t_life  = ', sum(tee_table*f_0)*d_iks
  write(107, '(A11,E11.4)') '% t_life  = ', sum(tee_table*f_0)*d_iks
endif

! writing headers of outputs
! writing epsilon_0 values
if(iteration_diag_heavy .NE. 0)then
  write(104,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") 0., e_table*511.
 ! write(104,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") 0., f_table  
  write(105,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") 0., e_table*511.
  write(106,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") 0., e_table*511.
  write(107,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") 0., e_table*511.
endif
if(iteration_diag .NE. 0)then
  write(102, '(15A14)') 'time [ps]', 'radius [µm]', 'lambdaD [µm]', 'Efr [keV]', 'Ere [keV]',&
                      'Q in [nC]', 'pot th [keV]', 'pot E [keV]', 'J fr [kA]', 'J re [kA]',&
                      'Q [nC]', 'E_cloud [J]', 'E_heated [J]', 'E_ejected [J]','(E_c+E_h)/all'
endif


! initialisating scalars and counters
getout = .false.
t = 0.
i_phi = N_iks+1

heat_matter = 0.

J = 0
counter=1
counter_diag=1
f_table = 0.
mheating_table = 0.
allocate(J_time(1:ceiling(t_max/dt)),R_time(1:ceiling(t_max/dt)),Time(1:ceiling(t_max/dt)))


!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%% MAIN WHILE LOOP START %%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
do while(getout .eqv. .false.)
  t=t+dt        !one step forward !
  !$OMP PARALLEL
  !$OMP DO
  do i_iks = 1,N_iks  

    ! feed f by the laser with electrons issued from f_0
    if(t<t_laser)then 
      f_table(i_iks) =  f_table(i_iks) + dt*( (10**3/0.511) * C_abs * E_laser/Em0/t_laser *f_0(i_iks) ) !(first term of eq. 1)
    endif
    
    r_table(i_iks) = min(rmax_table(i_iks), t*v_table(i_iks))*C_slow(i_iks)      ! (eq 8)
    z_table(i_iks) = min(rmax_table(i_iks), t*v_table(i_iks))                    ! (eq 8)

    if(r_table(i_iks) > d_targe/2.)then
      sfr_table(i_iks) = pi * (d_targe/2.)**2.    
      thetafr_table(i_iks) = pi/2.  ! (eq 12b)
      if(z_table(i_iks) > e_targe)then  
        sre_table(i_iks) = sfr_table(i_iks)
        Vol_table(i_iks) = pi * (d_targe/2.)**2. * e_targe   ! (eq 9b)
        thetare_table(i_iks) = pi/2.  ! (eq 12b)   
      else
        sre_table(i_iks) = 0.
        Vol_table(i_iks) = pi * (d_targe/2.)**2. * z_table(i_iks)   ! (eq 9b)
        thetare_table(i_iks) = 0.  ! (eq 12b)   ! MEmx CHANGE 'thetafr_table' TO 'thetare_table'
      endif 
    else
      sfr_table(i_iks) = pi * (r_laser + r_table(i_iks))**2.
      if(xE_table(i_iks) < e_targe)then
        ! case if thick target, front face.   
        thetafr_table(i_iks) = atan(r_table(i_iks)/xE_table(i_iks))  ! (eq 12b)

        ! case if thick target, rear face.     
        if(z_table(i_iks) > e_targe)then
          sre_table(i_iks) = sfr_table(i_iks)
          Vol_table(i_iks) = pi * (r_laser + r_table(i_iks))**2. * e_targe  ! (eq 9b)    
          thetare_table(i_iks) =  atan(r_table(i_iks) / ( e_targe - xE_table(i_iks)))
        ! reach_re(i_iks) = .true.       
        else
          sre_table(i_iks) = 0.     ! (eq 9a)   
          Vol_table(i_iks) = pi * (r_laser + r_table(i_iks))**2. * z_table(i_iks)   ! (eq 9b)           
          thetare_table(i_iks) = 0. ! (eq 12b)  
        ! reach_re(i_iks) = .false.        
        endif     
      else
        ! case if thin target, front face.
          thetafr_table(i_iks) = atan( r_table(i_iks) / e_targe ) ! (eq 12b)  
        ! case if thin target, rear face.     
        if(z_table(i_iks) > e_targe)then     
          sre_table(i_iks) = sfr_table(i_iks)
          Vol_table(i_iks) = pi * (r_laser + r_table(i_iks))**2. * e_targe   ! (eq 9b)              
          thetare_table(i_iks) = pi/2.
      ! reach_re(i_iks) = .true.      
        else
          sre_table(i_iks) = 0.     ! (eq 9a)   
          Vol_table(i_iks) = pi * (r_laser + r_table(i_iks))**2. * z_table(i_iks)   ! (eq 9b)             
          thetare_table(i_iks) = 0. ! (eq 12b)   
     ! reach_re(i_iks) = .false.        
        endif   
      endif
    endif
  
    mheating_table(i_iks) = max(0.,f_table(i_iks)) * dt / tee_table(i_iks) ! losses to ambient matter
    f_table(i_iks) = f_table(i_iks)*max(0., 1.  - dt / tee_table(i_iks) ) !(second term of eq. 1)
    f_table(i_iks) = max(0., f_table(i_iks) - dt*gfr_table(i_iks) - dt*gre_table(i_iks)) !(third term of eq. 1)

  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  ! calul of scalar for the potential
  N = sum(f_table)*d_iks                          ! number of hot electron (eq 10a)
  Em = sum(f_table*e_table)*d_iks / N             ! temperature (eq 11)
  Rav = sum(f_table*min(r_laser + r_table, d_targe/2.))*d_iks / N  ! cloud radius average (limited by target radius)
  vol = sum(f_table*vol_table)*d_iks/N            ! cloud volume average (eq 10b)
  ne  = w_laser**2*(10./(1.6*1.1))* N/vol         ! cloud density normalised to critical density
  LambdaD =  w_laser*sqrt(Em/ne) / (2*pi)         ! hot electron Debyelenght
  RLD = (r_laser + Rav)/LambdaD                   ! average electronic range / hot electron Debyelenght
  eLD = e_targe/LambdaD                           ! target thickness / hot electron Debyelenght
  
  ! calculate energy fractions
  heat_ejected = heat_ejected + sum(dt*(gfr_table+gre_table)*e_table)*d_iks*0.511*1.e-3
  heat_matter = heat_matter + sum(mheating_table*e_table)*d_iks*0.511*1.e-3
  heat_cloud = sum(f_table*e_table)*d_iks*0.511*1.e-3

  ! calculate the phi barrier
  call phi_calc

  gfr_table = 0.             ! (eq 15a) in kA/s/mec²
  gre_table = 0.             ! (eq 15b) in kA/s/mec²
  
  !$OMP PARALLEL
  !$OMP DO
  do i_iks = i_phi,N_iks
    gfr_table(i_iks) = (sin(thetafr_table(i_iks)/2.)**2)*v_table(i_iks) &
                        * f_table(i_iks) * sfr_table(i_iks)/ vol_table(i_iks)
    gre_table(i_iks) = (sin(thetare_table(i_iks)/2.)**2)*v_table(i_iks) &
                        * f_table(i_iks) * sre_table(i_iks)/ vol_table(i_iks)
  enddo
  !$OMP END DO
  !$OMP END PARALLEL

  J= sum(gfr_table + gre_table)*d_iks   ! (eq 16a)
  Q = Q + dt*J                          ! (eq 16b)
   
  !increasing iteration counter for potential calulation. and for diag
  counter = counter+1  

  ! out_of_while conditionners
  if(t>t_max)then !arbitrary time limit
    getout=.true.
    print*, 'time max reached'
  endif

  if(Em<0)then !hot electron cooling over.
    getout=.true.
    print*, 'negative temperature reached'
  endif

   if(N<0.001*Q .and. t>t_laser)then !hot electron cooling over.
    getout=.true.
    print*, 'empty electron cloud'
  endif 
 
  if(isnan(J))then !ejection total.
    getout=.true.
    print*, 'NaN J reached'
  endif
  
  ! writing scalar diagnotics
  
  if(iteration_diag .NE. 0)then
    if(MODULO(counter_diag,iteration_diag) == 0 )then
      if(.not.getout)then
        write(102, '(15ES14.4E3)') t, Rav, LambdaD, Em*511, sum(f_table*e_table*(1-r_table/rmax_table)**(3./5.))*d_iks*511 / N  &
        , N, phi_th*Em*511, phi_E*Em*511,sum(gfr_table)*d_iks, sum(gre_table)*d_iks, Q, heat_cloud, heat_matter, heat_ejected, &
        (heat_cloud+heat_matter)/(heat_ejected+heat_cloud+heat_matter)
        flush(102)
        ! PRINT*,"WRITE OUT SCALAR DIAG AT ",t," WITH ",Q,"nC"
      endif
    endif
  endif
  counter_diag = counter_diag + 1 
  
  ! writing distrib function diagnotics
  if(iteration_diag_heavy .NE. 0)then
    if(counter_diag_heavy ==10**(min(floor(max(log10(t/dt),1.)),3)) )then
      counter_diag_heavy =1
      if(.not.getout)then
        write(104,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") t, f_table/511. ! output unit is nC/keV
        write(105,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") t, gfr_table/511. ! output unit is nC/keV
        write(106,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") t, gre_table/511. ! output unit is nC/keV
        write(107,FMT= "((E15.4),"//trim(adjustl(N_iks_string))//"(ES14.4E3))") t, (mheating_table*e_table)*(d_iks*511.) / 6.242e15 ! output unit is J
        flush(104)
        flush(105)
        flush(106)
        flush(107)
        ! PRINT*,"WRITE OUT HEAVY DIAG AT ",t," WITH TEE HEATING BY ",sum(mheating_table*e_table)*d_iks*511./ 6.242e15," J"
      endif
    else
      counter_diag_heavy = counter_diag_heavy+1 
    endif 
  endif
  
enddo

! MEmx INSERT 543: Write Information into Log File
call logfile
! END 543

!run chronometer stop
call CPU_TIME(cpu_T2) 
print*, 'I found ', Q, ' nC'
print*, ' '

!close input files and output files, deallocate all.
if(iteration_diag .NE. 0)then
  close(101)
  close(102)
endif
if(iteration_diag_heavy .NE. 0)then
  close(104)
  close(105)
  close(106)
  close(107)
endif

! deallocating things.
!deallocate(iks_table,f_table,rmax_table,f_0, &
   !      r_table, r1_table, vol_table, &
   !      e_table, v_table,e1_table, v1_table, &
   !      xE_table, tee_table,&
   !      sfr_table, sre_table, gfr_table, gre_table,&
   !      thetafr_table,thetare_table)
!deallocate(J_time,R_time,Time)

!short screen message with run time
print*, 'I run during',  (cpu_T2 - cpu_T1)/60.,' min'


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!! SUBROUTINES !!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

contains

subroutine Phi_calc
use parameters, only : J,t,dt,counter,pi,t_max,r_laser, phi, N,LambdaD, vol,RLD
use parameters, only : iks_table, N_iks, i_phi, phi_E, phi_th
use parameters, only : cond, d_targe, e_targe, phi_E_plugged
use parameters, only : J_time, R_time, Time
implicit none
real*8 :: c = 300. !speedlight
real*8 :: zero=0.
real*8 :: Rp
integer :: k,iks,it

!calulation of thermal potential and minimal potential position.
call Phi_th_interp(phi_th,RLD,eLD)

if(phi_E_plugged)then
  !!!filling table of J, R, time
  J_time(counter) = J
  R_time(counter) = RLD*LambdaD
  Time(counter) = t

  phi_E = 0 !cumulated contribution of the electrostatic potential from the current and radius at time n between 0 to t.

  ! It is normalised to Temp. calculated at surface position.
  !!conductive target electrostatic potential
  if(cond == 1.)then
    do k=1,counter
      Rp = min(R_time(k) + c*(t - Time(k)),d_targe) ! electrostatic potential radius
      phi_E = phi_E + J_time(k) *dt * 0.5 *vol / ( pi*Rp*N*LambdaD**2)
    enddo
  
  !!dielectric target electrostatic potential
  elseif( cond == 0.) then
    do k=1,counter
      Rp = R_time(k) ! electrostatic potential radius (no propagation because no conductivity)
      phi_E = phi_E + J_time(k) *dt * 0.5 *vol / ( pi*Rp*N *LambdaD**2)
    enddo    
  endif

  !!summation of potential electrostatic and thermal.
  phi =  phi_E + phi_th

else
  phi =  phi_th  
endif


!research of the barrier index i_phi in the table iks
do iks = 1,N_iks
  if(e_table(iks) .GT. phi*Em) exit
enddo
i_phi = iks-1


end subroutine Phi_calc
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine Phi_th_interp(phi_max_out,RLDin,eLDin)
use parameters, only : t
implicit none
integer :: i1,i2,i
real*8 :: z,l,E, alpha, b, bpE, phi_max
logical, save :: first_time = .true.
real*8,intent(out) :: phi_max_out
real*8,intent(in) :: RLDin, eLDin

! thick thermal formula (eq 18)
phi_max =0
phi_max_out=0
l=sqrt(2.*exp(1.))/RLDin
z = 0.
phi_max_out = (l+z)/(sqrt((l+z)**2.+1.))*log((sqrt((z+l)**2.+1.)*sqrt(z**2.+1.)+1.+z*(z+l))/(l*(sqrt((z+l)**2.+1.)-z-l))) &
                                                  -asinh(z) -log(2.) -2.*log(l+z)+log(l)
                                                  
!thin target phi_th calcul (eq 20 & 21)
if(eLDin < 1.) then
  alpha = 2./eLDin**2. *(sqrt(1.+eLDin**2./2.)-1.)
  E=eLDin/RLDin
  b =2./(eLDin*RLDin*alpha)-E/2.
  bpE = 2./(eLDin*RLDin*alpha)
  phi_max = alpha*RLDin**2.*(E/2.*(sqrt(E**2./4.+1.)-E/2.) +asinh(E/2.)) &
    -2.*( +log(2.) -1. + b/(sqrt(b**2.+1.))*log(sqrt(b**2.+1.)-b)) &
    +2.*(b/(sqrt(b**2.+1.))*log((sqrt(b**2.+1.)*sqrt(E**2./4.+1.)-b*E/2.+1.)/(bpE)) &
    - sqrt(E**2./4.+1.)/bpE &
    - b/bpE - log(bpE) +asinh(E/2.))
  phi_max = phi_max*(1-alpha*eLDin**2./4)

  if(phi_max_out > phi_max) then ! comparison of possible potentials.
    phi_max_out = phi_max
  endif
endif

end subroutine Phi_th_interp

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!! list of materials !!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine metal_choice
use parameters, only : Z_target,rho_target, A_target, cond, metal
implicit none

select case (metal)
case(1)
  Z_target   = 6    !carbone
  rho_target = 2.00 !carbone
  A_target   = 12.0 !carbone
  cond       = 0. 
case(2)
  Z_target   = 13   !aluminum
  rho_target = 2.70 !aluminum
  A_target   = 27.0 !aluminum
  cond       = 1. 
case(3)
  Z_target   = 29   !cuivre
  rho_target = 8.96 !cuivre
  A_target   = 63.5 !cuivre
  cond       = 1. 
case(4)
  Z_target   = 74   !tungstene
  rho_target = 19.30!tungstene
  A_target   = 183.8!tungstene
  cond       = 1. 
case(5)
  Z_target   = 73     !tantale
  rho_target = 16.4   !tantale
  A_target   = 180.94 !tantale
  cond       = 1. 
case(6)
  Z_target   = 82    !plomb
  rho_target = 11.35 !plomb
  A_target   = 207   !plomb
  cond       = 1. 
case(7)
    Z_target   = 79   !gold
    rho_target = 19.3 !gold
    A_target   = 196.9 !gold
    cond       = 1.
case(8)
    Z_target   = 47     !silver
    rho_target = 10.5   !silver
    A_target   = 107.87 !silver
    cond       = 1.
case(10)
    Z_target   = 8    !teflon
    rho_target = 2.16 !teflon
    A_target   = 16.0 !teflon
    cond       = 0. 
case(11)
    Z_target   = 8    !ionized teflon
    rho_target = 2.16 !ionized teflon
    A_target   = 16.0 !ionized teflon
    cond       = 1. 
case(12)
  Z_target   = 6    !carbone
  rho_target = 2.00 !carbone
  A_target   = 12.0 !carbone
  cond       = 1. 
case(13)
  Z_target   = 26      !fer
  rho_target = 7.874   !fer
  A_target   = 55.845  !fer
  cond       = 1.
case(14)
  Z_target   = 22     !titanium
  rho_target = 4.506  !titanium
  A_target   = 47.867 !titanium
  cond       = 1.
case(15)
  Z_target   = 78      !platinum
  rho_target = 21.5    !platinum
  A_target   = 195.084 !platinum
  cond       = 1.
case(16)
    Z_target   = 6    !kapton
    rho_target = 1.42 !kapton
    A_target   = 12   !kapton
    cond       = 0. 
  
end select

end subroutine metal_choice
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!! File manager for output !!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
subroutine files_manager
use parameters
implicit none
character(len=14) :: file_name_in
logical :: file_exists
!read input file, create output files with check and warnings...

if(iargc()==0)then! check if argument is there.
  print*, 'no input file number. I run with default value and create file 9999'
  numbfile = 9999
else
  call getarg(1,char_a)
  read(char_a , *) numbfile
  write(file_name_in , '(A6,A4,A4)') 'input_',char_a,'.dat'
  INQUIRE(FILE=file_name_in, EXIST=file_exists)
  if(file_exists .eqv. .false.)then
    print*,file_name_in
    print*, 'I got file number ',char_a,'. But I can''t find the input file.'        
    print*, 'I''ll look for a defautl file 0000'
    write(file_name_in , '(A14)') 'input_0000.dat'
    INQUIRE(FILE=file_name_in, EXIST=file_exists)
    if(file_exists .eqv. .true.)then
      call read_input_file(0000)
    else
      print*, 'No default input file 0000. I will use default parameters.'
    endif
  else
    call read_input_file(numbfile)       
  endif
end if

end subroutine files_manager
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
subroutine create_output_file(g)
use parameters
implicit none
LOGICAL :: file_exists=.true.
integer, intent(in) :: g
integer :: f
character(len=23) :: file_name
! create output files and copy input file as reminder.

if(iteration_diag .NE. 0) then
  write(file_name , '(A14,I4.4,A4)') 'output_scalar_',g,'.txt'
  INQUIRE(FILE=trim(file_name), EXIST=file_exists)
  if(file_exists)then
    print*, 'FAILURE: There is already some output here. Change the input number or remove the output file!'
    stop 20
  end if
  open(102, file=file_name, status = 'new', form = 'formatted')
  if(details(102)) PRINT*,"START ",file_name
endif
if(iteration_diag_heavy .NE. 0) then
  write(file_name , '(A14,I4.4,A4)') 'output_f_in___',g,'.txt'
  open(104, file=file_name, status = 'new', form = 'formatted')
  if(details(104)) PRINT*,"START ",file_name
  
  write(file_name , '(A14,I4.4,A4)') 'output_f_Jfr__',g,'.txt'
  open(105, file=file_name, status = 'new', form = 'formatted')
  if(details(105)) PRINT*,"START ",file_name
  
  write(file_name , '(A14,I4.4,A4)') 'output_f_Jre__',g,'.txt'
  open(106, file=file_name, status = 'new', form = 'formatted')
  if(details(106)) PRINT*,"START ",file_name
  
  write(file_name , '(A14,I4.4,A4)') 'output_f_heat__',g,'.txt'
  open(107, file=file_name, status = 'new', form = 'formatted')
  if(details(107)) PRINT*,"START ",file_name
  
end if

end subroutine create_output_file

function details(num_unit)

  use parameters

    integer :: num_unit
    logical :: details
    
    write(num_unit, '(A11,I4)') '% metal   = ', metal
    write(num_unit, '(A11,I6)') '% N_iks   = ', N_iks 
    write(num_unit, '(A11,E11.4)') '% dt      = ', dt
    write(num_unit, '(A11,I6)') '% Nb Th   = ', Nb_Th
    write(num_unit, '(A11,I6)') '% diag s  = ', iteration_diag
    write(num_unit, '(A11,I6)') '% diag f  = ', iteration_diag_heavy
    write(num_unit, '(A11,E11.4)') '% t_laser = ', t_laser
    write(num_unit, '(A11,E11.4)') '% E_laser = ', E_laser
    write(num_unit, '(A11,E11.4)') '% C_abs   = ', C_abs
    write(num_unit, '(A11,E11.4)') '% w_laser = ', w_laser
    write(num_unit, '(A11,E11.4)') '% d_laser = ', d_laser
    write(num_unit, '(A11,E11.4)') '% d_targe = ', d_targe
    write(num_unit, '(A11,E11.4)') '% e_targe = ', e_targe
    write(num_unit, '(A12,L1)')    '% T_isol  = ', phi_E_plugged
    
    details = .True.

end function details
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! MEmx ADD INTO NEW 847: Write log file content
subroutine logfile
use parameters
implicit none
LOGICAL :: file_exists=.true.
! create output files and copy input file as reminder.
inquire(file="log.txt", exist=file_exists)
  if (file_exists) then
    open(200, file="log.txt", status="old", position="append", action="write")
  else
    open(200, file="log.txt", status="new", action="write")
  end if
  write(200, *) metal,N_iks,dt,Nb_Th,&
                &t_laser,E_laser,C_abs,w_laser,d_laser,d_targe,e_targe,Q
  close(200)
end subroutine logfile
!%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
! END ADD INTO NEW 847
subroutine read_input_file(g)
use parameters
implicit none
LOGICAL :: file_exists=.true.
integer, intent(in) :: g
character(len=14) :: file_name
character(len=11) :: trash
!read input file and fills data for the run.
write(file_name , '(A6,I4.4,A4)') 'input_',g,'.dat'
open(1, file=file_name, status = 'old', form = 'formatted')

read(1, '(A11,I4)')    trash, metal
read(1, '(A11,I6)')    trash, N_iks 
read(1, '(A11,E11.4)') trash, dt
read(1, '(A11,I6)')    trash, Nb_Th
read(1, '(A11,I6)')    trash, iteration_diag
read(1, '(A11,I6)')    trash, iteration_diag_heavy
read(1, '(A11,E11.4)') trash, t_laser
read(1, '(A11,E11.4)') trash, E_laser
read(1, '(A11,E11.4)') trash, C_abs
read(1, '(A11,E11.4)') trash, w_laser
read(1, '(A11,E11.4)') trash, d_laser
read(1, '(A11,E11.4)') trash, d_targe
read(1, '(A11,E11.4)') trash, e_targe
!read(1, '(A12,L1)')    trash, phi_E_plugged ! MEmx COMMENT THIS LINE OUT
end subroutine read_input_file
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
end program charge_target

