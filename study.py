# -*- coding: utf-8 -*-
"""
Choccolat Study {um,ps,nC,J}

This script is a pilot for parametric studies with the
ChoCoLaT - 2 code of Alexandre Poyé.

After import of modules, and general settings, the
runtime parameters are set! Please adjust before run!

"""

# ==============================================================================
# IMPORT MODULES
# ==============================================================================
# External libraries

import matplotlib.style as style
import matplotlib.pyplot as plt
from matplotlib import cm
from matplotlib.colors import LogNorm
import numpy as np
import pandas as pd

from scipy.optimize import curve_fit
from scipy.interpolate import griddata

import math
     
import subprocess
import os

import shlex

# ==============================================================================
# STYLE PARAMETERS
# ==============================================================================
# Overwrite style for plots
style.use('default')
plt.rcParams['errorbar.capsize'] = 3
plt.rcParams["font.family"] = "Times New Roman"
plt.rcParams["font.size"] = "22"
plt.rc('mathtext', fontset="cm")
plt.tick_params(axis = 'both', which = 'major', labelsize = 22)
plt.rcParams['figure.figsize'] = [8,4]
plt.rcParams['figure.dpi'] = 80
plt.rcParams['savefig.dpi'] = 600
#plt.axes().set_aspect(5.0)

# ==============================================================================
# PARAMETERS
# ==============================================================================
# Constants
vc = 299.792458

fortran_file = "ChoCoLaT177_v2_hotspots.f90"

# What to do?
simulate =  1          # .. undertake a new simulation: 1 :: True | 0 :: False
numintegercount = 468  # .. with number

plot = 0               # .. and plot results: 1 :: True | 0 :: False
pilotorplotter = 1     # .. refinement: 1 :: pilot and analysis mode | 2 :: final plotter mode

# Simulation parameters: simulate == 1 ? create raster of test cases
metal_container   =  [3]      # 1:carbon 2:aluminum 3:cuivre 4:wolfram 5:tantal 6:plumbium 7:aurum 8:argentum 9:nickel 10:teflon 13:Fe 15:Pt 14:Ti 15:Platinium 16:Kapton 17:conductiveKapton
N_iks_container   =  [20000]  # number of bins for the electron distribution function, discretisation in mec**2
dt_container      =  [0.01]  # time step for initial value solver, in ps
Nb_Th_container     =  [20]   # Max N*(T_h in [eV]), is energy maximum of the distribution function as Nb_th * Th
diag_s    =  100              # save scalar diagnostic every Nth iteration, 0 is setting the diagnostics off
diag_f    =  0                # save distribution function diagnostic every Nth iteration, 0 is setting the diagnostics off

t_laser_container =  [350.]   # duration of the laser pulse, in ps (FWHM ! not clear if this is always consistent)
E_laser_container =  [600.]     # energy of the laser pulse, in J => P_laser = E_laser / t_laser
C_abs_container   =  [0.5]     # absorbtion coefficent of the laser pulse, from 0 to 1 (! not taken into account in Beg's law)

w_laser_container =  [1.314]  # wavelength of the laser pulse, in um
d_laser_container =  [100.]   # diameter of the laser spot, in um => r_laser = d_laser/2./sqrt(log(2.)) => P_laser  / ( pi* r_laser**2)
d_targe_container =  [3000.]  # diameter of the target, in um
e_targe_container =  [50.]    # thickness of the target, in um

all_combinations = True # permute all of above if True, expect equally long lists and iterate if False (fills up with last value)

# Plotter: plot == 1 ? adjust plots
showplots = 0      # prompt plots: 1 :: True | 0 :: False
convergence = 0.99 # demand agreement to that relative degree TODO

confinement = 0.99 # increase width of search stripe to that percentage in contrast plots

workmetal = 3  # material to be processed in plots (important if many materials were rastered over)

trymetric = 0  # get metric related plots
dometric = 0   # overwrite laser diameter container with diameters according to expansionratio of following test case
dorandom = 0   # d_laser_container =  [#from,#+range,#randompoints]

plotrange_tL = [0.3,2.0]
plotrange_rL = [15.,35.]

#test case, to be highlighted in plots
frL = 25.0 #laser spot radius
ftL = 0.5 #laser pulse duration
fEL = 50. #laser pulse energy

fQ0 = 120.#try to elaborate rL and tL in order to find this experimental Q0 with only EL

chocholatfactor = 2.5       # factor to which chocolat underestimates the discharge usually compared to experiments
cocolatfactorerror = 0.5    # error on this factor

vg = 1.0  * vc                    # auto : pulse propagation or surface potential expansion speed
vgtlrl = round(vg * ftL / frL,2)  # auto : comparative set value of potential expantion metric

# If only plot is wished, the ratios can be chosen in a list (or [None])
plotratios = [None] # max 7

# If only plots are wished, the indication of evaluation points can be chosen
showevalpoints = False

# ==============================================================================
# FUNCTIONS
# ==============================================================================

def available_cpu_count():
    """ https://stackoverflow.com/a/1006301
    Number of available virtual or physical CPUs on this system, i.e.
    user/real as output by time(1) when called with an optimally scaling
    userspace-only program"""
    import re
    # cpuset
    # cpuset may restrict the number of *available* processors
    try:
        m = re.search(r'(?m)^Cpus_allowed:\s*(.*)$',
                      open('/proc/self/status').read())
        if m:
            res = bin(int(m.group(1).replace(',', ''), 16)).count('1')
            if res > 0:
                return res
    except IOError:
        pass

    # Python 2.6+
    try:
        import multiprocessing
        return multiprocessing.cpu_count()
    except (ImportError, NotImplementedError):
        pass

    # https://github.com/giampaolo/psutil
    try:
        import psutil
        return psutil.cpu_count()   # psutil.NUM_CPUS on old versions
    except (ImportError, AttributeError):
        pass

    # POSIX
    try:
        res = int(os.sysconf('SC_NPROCESSORS_ONLN'))

        if res > 0:
            return res
    except (AttributeError, ValueError):
        pass

    # Windows
    try:
        res = int(os.environ['NUMBER_OF_PROCESSORS'])

        if res > 0:
            return res
    except (KeyError, ValueError):
        pass

    # jython
    try:
        from java.lang import Runtime
        runtime = Runtime.getRuntime()
        res = runtime.availableProcessors()
        if res > 0:
            return res
    except ImportError:
        pass

    # BSD
    try:
        sysctl = subprocess.Popen(['sysctl', '-n', 'hw.ncpu'],
                                  stdout=subprocess.PIPE)
        scStdout = sysctl.communicate()[0]
        res = int(scStdout)

        if res > 0:
            return res
    except (OSError, ValueError):
        pass

    # Linux
    try:
        res = open('/proc/cpuinfo').read().count('processor\t:')

        if res > 0:
            return res
    except IOError:
        pass

    # Solaris
    try:
        pseudoDevices = os.listdir('/devices/pseudo/')
        res = 0
        for pd in pseudoDevices:
            if re.match(r'^cpuid@[0-9]+$', pd):
                res += 1

        if res > 0:
            return res
    except OSError:
        pass

    # Other UNIXes (heuristic)
    try:
        try:
            dmesg = open('/var/run/dmesg.boot').read()
        except IOError:
            dmesgProcess = subprocess.Popen(['dmesg'], stdout=subprocess.PIPE)
            dmesg = dmesgProcess.communicate()[0]

        res = 0
        while '\ncpu' + str(res) + ':' in dmesg:
            res += 1

        if res > 0:
            return res
    except OSError:
        pass

    raise Exception('Can not determine number of CPUs on this system')

# ==============================================================================
# MAIN RUN
# ==============================================================================
# see cores of computer
cpu_count = available_cpu_count()
use_fraction = 0.4
use_cpu = int(use_fraction*cpu_count)

# create job list
job_list = []
if all_combinations:
    for metal in metal_container:
        for N_iks in N_iks_container:
            for dt in dt_container:
                for Nb_Th in Nb_Th_container:
                    for t_laser in t_laser_container:
                        for E_laser in E_laser_container:
                            for C_abs in C_abs_container:
                                for w_laser in w_laser_container:
                                    for d_laser in d_laser_container:
                                        for d_targe in d_targe_container:
                                            for e_targe in e_targe_container:
                                                job_list.append([metal,N_iks,dt,Nb_Th,t_laser,E_laser,C_abs,w_laser,d_laser,d_targe,e_targe])
else:
    max_len = max(len(metal_container),len(N_iks_container),len(dt_container),len(Nb_Th_container),len(t_laser_container),len(E_laser_container),len(C_abs_container),len(w_laser_container),len(d_laser_container),len(d_targe_container),len(e_targe_container))
    
    def fill_up(li,targetl):
        lil = len(li)
        if lil < targetl:
            for i in range(targetl - lil):
                li.append(li[lil-1])
        return li
        
    metal_container = fill_up(metal_container,max_len)
    N_iks_container = fill_up(N_iks_container,max_len)
    dt_container = fill_up(dt_container,max_len)
    Nb_Th_container = fill_up(Nb_Th_container,max_len)
    t_laser_container = fill_up(t_laser_container,max_len)
    E_laser_container = fill_up(E_laser_container,max_len)
    C_abs_container = fill_up(C_abs_container,max_len)
    w_laser_container = fill_up(w_laser_container,max_len)
    d_laser_container = fill_up(d_laser_container,max_len)
    d_targe_container = fill_up(d_targe_container,max_len)
    e_targe_container = fill_up(e_targe_container,max_len)
    
    for metal,N_iks,dt,Nb_Th,t_laser,E_laser,C_abs,w_laser,d_laser,d_targe,e_targe in zip(metal_container,N_iks_container,dt_container,Nb_Th_container,t_laser_container,E_laser_container,C_abs_container,w_laser_container,d_laser_container,d_targe_container,e_targe_container):
        job_list.append([metal,N_iks,dt,Nb_Th,t_laser,E_laser,C_abs,w_laser,d_laser,d_targe,e_targe])

# Get Going
numintegercount = numintegercount - 1
# PGS
print("Coucou - let's do some ChoCoLaT")
print(" ")
cwd = os.getcwd()
print(' WORK in '+cwd)
print(" ")
print(' CPUs    '+str(cpu_count))
print(' try on  '+str(use_cpu))
print(" ")
                    
if pilotorplotter == 1 :

    print(' WORK on '+str(vgtlrl))
    print(" ")

    # Simulate Cases
    if simulate == 1:
        #compile chocolat
        commandline = ['gfortran','-O3 "'+cwd+os.path.sep+fortran_file+'"','-o chch','-fopenmp']
        print(' CALL ' + " ".join(commandline))
        cmd = commandline
        p = subprocess.Popen(cmd,stdout=subprocess.PIPE, shell=True)
        p.wait()

        # Go through a cases and launch Chocolat
        p = []
        print(" Setup ChoCoLaT..")
        launched_first = False
        
        j = 0
        job_batches = []
        for job_i in job_list:
            j = j+1
            if j == 1:
                job_batches.append([])
            job_batches[-1].append(job_i)
            if j >= use_cpu:
                j = 0
                
        print(' BATCHES '+str(len(job_batches)))
        print(" ")
        
        for b,batch in enumerate(job_batches):
            print(" ")
            print(' > BATCH '+str(b+1))
            for job_i in batch: 
                metal,N_iks,dt,Nb_Th,t_laser,E_laser,C_abs,w_laser,d_laser,d_targe,e_targe = job_i
                #Do we investigate a specific metric?
                if dometric == 1:      # diameter = 2 * radius_i (t,ratio,vg) | do i, 0.5,1.5,0.5
                    d_laser_container = [2.*vg*t_laser/(vgtlrl*(1.+float(i)/100.)) for i in range(-1,2)]
                elif dorandom == 1:
                    d_laser_container = [d_laser_container[0] + item * d_laser_container[1] for item in np.random.rand(int(d_laser_container[2]))]
                # start
                numintegercount = numintegercount + 1
                #if numintegercount <= 456:
                #    continue
                numstring = '%04d' % numintegercount
                filename = "input_"+numstring+".dat"
                ifile = open(filename,"w") 
                ifile.write('metal   =  '+str(metal)+"\n")
                ifile.write('N_iks   =  '+str(N_iks)+"\n")
                ifile.write('dt      =  '+str(dt)+"\n")
                ifile.write('Nb_Th     =  '+str(Nb_Th)+"\n")
                ifile.write('diag_s    =  '+str(diag_s)+"\n")
                ifile.write('diag_f    =  '+str(diag_f)+"\n")
                ifile.write('t_laser =  '+str(t_laser)+"\n")
                ifile.write('E_laser =  '+str(E_laser)+"\n")
                ifile.write('C_abs   =  '+str(C_abs)+"\n")
                ifile.write('w_laser =  '+str(w_laser)+"\n")
                ifile.write('d_laser =  '+str(d_laser)+"\n")
                ifile.write('d_targe =  '+str(d_targe)+"\n")
                ifile.write('e_targe =  '+str(e_targe))
                ifile.close()
                try:
                    if launched_first == False:
                        print(" ChoCoLaT at work..")
                        launched_first = True
                    if os.path.sep == '\\':
                        commandline = [cwd+os.path.sep+'chch.exe',numstring]
                    else:
                        commandline = [os.path.sep+cwd+os.path.sep+'chch',numstring]
                    
                    print(' DO '+' '.join(commandline))
                    cmd = commandline
                    p.append([filename,subprocess.Popen(cmd,stdout=subprocess.PIPE, shell=True, cwd=cwd+os.path.sep)])
                except:
                    print(" Execution failed for "+numstring)
                    if os.path.isfile(filename):
                        os.remove(filename)
                        print(" Delete: %s file not found" % filename)
                    else:
                        print(" Error: %s file not found" % filename)
                                                            
            
            exit_codes = [pi.wait() for name,pi in p]
            print(" ..EXIT with")
            for exit in range(len(exit_codes)) :
                print(" ID :: "+str(exit)+" => CODE :: "+str(exit_codes[exit]))
                if os.path.isfile(p[exit][0]):
                    os.remove(p[exit][0])

    # Plot Cases                
    if plot == 1:                
        # fetch logfile
        with open(cwd+os.path.sep+'log.txt',"r") as ofile:
            content = ofile.readlines()
        content = [x.strip(' \r\n\t').split() for x in content]
        # Container for Data
        metal_container = []
        N_iks_container = []
        dt_container = []
        Nb_Th_container = []
        t_laser_container = []
        E_laser_container = []
        C_abs_container = []
        w_laser_container = []
        d_laser_container = []
        d_targe_container = []
        e_targe_container = []
        Q_container = []
        # Parsa Data that is comparable materialwise
        for line in content:
            if workmetal == float(line[0]):
                metal_container.append(float(line[0]))
                N_iks_container.append(float(line[1]))
                dt_container.append(float(line[2]))
                Nb_Th_container.append(float(line[3]))
                t_laser_container.append(float(line[4]))
                E_laser_container.append(float(line[5]) * float(line[6]))
                C_abs_container.append(float(line[6]))
                w_laser_container.append(float(line[7]))
                d_laser_container.append(float(line[8]))
                d_targe_container.append(float(line[9]))
                e_targe_container.append(float(line[10]))
                Q_container.append(float(line[11]))
        # Evaluate Data Energy Resolution
        r_laser = np.array(d_laser_container)/2./math.sqrt(math.log(2.)) #Gausz Spot
        t_laser = np.array(t_laser_container)*math.sqrt(math.pi)/ (2.*math.sqrt(math.log(2.))) #Gausz Pulse
        P_laser = np.array(E_laser_container)/t_laser
        I_laser = P_laser  / ( math.pi* r_laser**2)
        
        a0 = 0.853*np.array(w_laser_container)*10.*np.sqrt(I_laser)
        
        T_hot_e = []
        
        for a in a0:
            if a > 0.03 :
              T_hot_e.append(max(0.47*a**(2./3.), math.sqrt(1.+a**2)-1.))
            else :
              T_hot_e.append(3.*a**(4./3.))

        E_res = np.array(Nb_Th_container) * np.array(T_hot_e) / np.array(N_iks_container)
        
        #TODO kick out all above a given minresolution
        
        # Run Tests of Convergence simulations - TODO
        #Test 1 Q1 with Nb_Th, N_iks, dt  (reference case)
        #Test 2 : Nb_Th, N_iks, 2*dt  (testing temporal dynamic)
        #Test 3 : Nb_Th, 2*N_iks, dt  (testing energy resolution)
        #Test 4 : 2*Nb_Th, 2*N_iks, dt (testing maximal energy value)
        # +> TEST OK IF ALL CHARGES AGREE
        
        # Plot Major Dependencies
        x = (t_laser * math.pi* r_laser**2)
        y = np.array(E_laser_container)
        z = np.array(Q_container)
        
        # Prepare Potential Expansion Metric
        xm = (vg * np.array(t_laser_container) / (np.array(d_laser_container)/2.))
        
        # Plot 3D
        
        #EXAMPLE
        #theta = 2 * np.pi * np.random.random(1000)
        #r = 6 * np.random.random(1000)
        #x = np.ravel(r * np.sin(theta))
        #y = np.ravel(r * np.cos(theta))
        #z = x*y
        #END EXAMPLE
        
        try:
        
            fig = plt.figure()
            
            ax = plt.axes(projection='3d')
            p1 = ax.plot_trisurf(x, y, z,cmap='viridis', edgecolor='none')
            
            p2 = ax.tricontourf(x, y, z, offset=-1, cmap=cm.coolwarm)
            
            ax.set_xlabel(r'$\tau_L \pi r_L^2$ / [ps um$^2$]')
            ax.set_ylabel(r'$E_L$ / [J]')
            ax.set_zlabel(r'$Q_0^C$ / [nC]')
            
            ax.xaxis._axinfo['label']['space_factor'] = 10.

            fig.colorbar(p1, shrink=0.5, aspect=5)
            
            plt.tight_layout()
            plt.savefig('comparative_3D_case'+str(workmetal)+'.png')
                
            if showplots == 1 : plt.show()
            plt.clf()
            
            # Do the Contour Only 2D
            fig = plt.figure()
            ax = plt.axes()
            
            p2 = ax.tricontourf(x, y, z, cmap=cm.coolwarm)

            ax.set_xlabel(r'$\tau_L \pi r_L^2$ / [ps um$^2$]')
            ax.set_ylabel(r'$E_L$ / [ J ]')

            fig.colorbar(p2, shrink=1., aspect=5, label=r'$Q_0^C$ / [nC]')
            
            plt.tight_layout()
            plt.savefig('comparative_2D_case'+str(workmetal)+'.png')
                
            if showplots == 1 : plt.show()
            plt.clf()
            
            # Do the Expansion Metric Plot
            fig = plt.figure()
            ax = plt.axes()
            
            p3 = ax.tricontourf(xm, z, y, cmap=cm.coolwarm)
            
            ax.set_xlabel(r'$v_g \tau_L / r_L$')
            ax.set_ylabel(r'$Q_0^C$ / [nC]')

            fig.colorbar(p3, shrink=1., aspect=5, label=r'$E_L$ / [ J ]')
            ax.axvline(x = vgtlrl, color='k')
            
            plt.tight_layout()
            plt.savefig('comparative_2D_metric_case'+str(workmetal)+'.png')

            if showplots == 1 : plt.show()
            plt.clf()
        
        except:
            print("not enought simulations for triangulation of results")
        
        # plot dependencies around fixed ratio in metric space
        if trymetric == 1:
        
            xf,yf,zf = [], [], []
            
            for xi,yi,zi,xj in zip(x,y,z,xm):
            
                if xj > convergence * vgtlrl and xj < (2.-convergence) * vgtlrl:
                
                    xf.append(xi)
                    yf.append(yi)
                    zf.append(zi)
        
            fig = plt.figure()
            ax = plt.axes()
            
            #ax.plot(xf, zf, 'ko', ms=3) #Control possibility for evaluation points #TODO optional
            divd = 5.
            miny = min(yf)
            maxy = max(yf)
            step = (max(yf)-min(yf))/divd
            
            if fEL < maxy and fEL > miny:

                root = (fEL-miny)/step
                if int(root) == 0 : 
                    miny=fEL
                elif int(root) == int(divd) : 
                    maxy=fEL
                else:
                    step = (fEL-miny)/round(root,0)
                
            levels =  np.arange(miny, maxy+step/2., step)    
            

                
           # levels =  np.sort(levels)
            
           # key = np.where(levels == fEL)
           # 
           # linestyles=[]
           # linewidths=[]
           # 
           # for level in levels:
           #     if level != fEL:
           #         linewidths.append(0.0)
           #     else:
           #         linewidths.append(1.0)
           #     linestyles.append('--')
            
            p4 = ax.tricontourf(xf, zf, yf, levels=levels, cmap='viridis')
            
            p5 = ax.tricontour(xf, zf, yf, levels=p4.levels[np.where(levels == fEL)], colors='r',linewidths=(2,),linestyles='--')
            
            ax.axvline(x = math.pi*ftL*frL**2, color='k')

            ax.set_xlabel(r'$\tau_L \pi r_L^2$ / [ps um$^2$]')
            ax.set_ylabel(r'$Q_0^C$ / [nC]')

            fig.colorbar(p4, shrink=1., aspect=5, label=r'$E_L$ / [ J ]')
            
            plt.title(r'$v_g \tau_L / r_L$ = '+str(vgtlrl))
            
            plt.tight_layout()
            plt.savefig('comparative_2D_fixedmetric_case'+str(workmetal)+'_vgtlrl-1_'+str(vgtlrl)+'.png')

            if showplots == 1 : plt.show()
            plt.clf()
            
            # Try to get an overview..
            # Fix E and search R (T) plot to reach Q_0
            # >> expect hyperbel { lim_inf=0; lim_0=inf}
            #
            # Which one leads to a maximum amplitude?
            #
            # Superpose this with R (vg*T/č) for many č
            # >> linear function with m=vg/č
            
            xRT,yRT,zRT = [], [], []
            
            for ri,ti,yi,zi in zip(np.array(d_laser_container)/2.,t_laser_container,y,z):
            
                if (yi > confinement * fEL and yi < (2.-confinement) * fEL) and (zi > confinement * fQ0/chocholatfactor and zi < (2.-confinement) * fQ0/chocholatfactor):
                
                    xRT.append(ti)
                    yRT.append(ri)
                    zRT.append(((fEL-yi)/(fEL+yi)+(fQ0/chocholatfactor-zi)/(fQ0/chocholatfactor+zi))*10.**(2)) #contrast to optimum
        
            fig = plt.figure()
            ax = plt.axes()
            
            if len(xRT) > 2 : p6 = ax.tricontourf(xRT, yRT, zRT, cmap=cm.coolwarm)
            
            ax.plot(xRT, yRT, 'ko', ms=3) #Control possibility for evaluation points #TODO optional
            
            factor = [1./10.,0.5,1.,2.,10.]
            for i in range(len(factor)):
                yl = [vg*t/(vgtlrl*factor[i]) for t in xRT]
                ax.plot(xRT, yl, '-',label=str(round(vgtlrl*factor[i],2)))
                #ax.scatter(xRT, yl, edgecolors='white')
                
            #    xpos = 0.8*max(xRT)
            #    count = 0
            #    for xi in xRT:
            #        if xi > xpos :
            #            count = count
            #        else:
            #            count = count  + 1
            #    ypos = count
            #   ax.annotate(str(round(vgtlrl*10.**(i),2)), xy=(xpos,yl[ypos]), xycoords='data')
        
            ax.set_xlabel(r'$\tau_L$ / [ps]')
            ax.set_ylabel(r'$r_L$ / [um]')

            if len(xRT) > 2 : fig.colorbar(p6, shrink=1.0, aspect=5, label=r'$\Sigma_i^{A \in \left( Q_0^C;E_L \right) } \left( \frac{A_i-A}{A_i+A} \right) $ / [%]')
            #ax.axvline(x = ftL, color='k')
            
            plt.title(r'$E_L $ = '+str(fEL)+r'J $Q_0^C$ = '+str(fQ0/chocholatfactor)+'nC')
            
            leg = ax.legend(loc='center right', borderaxespad=0., frameon=True, facecolor='white', framealpha = 0.8,
               ncol=1, prop={'size': 12})
               
            leg.set_title(r'$v_g \tau_L / r_L$', prop = {'size':12})
            
            if len(xRT) > 0 : ax.set_ylim([min(yRT),max(yRT)])
            
            plt.tight_layout()
            plt.savefig('comparative_2D_RTwith'+str(fQ0/chocholatfactor)+"nC"+str(fEL)+'J_case'+str(workmetal)+'.png')

            if showplots == 1 : plt.show()
            plt.clf()
            
            # Use the obtained knowledge to get a more generalized graph with multiple charges..
            # .. may fail or first runs as there is not enought data available
            
            fig = plt.figure()
            ax = plt.axes()
            
            xRTQ,yRTQ,zRTQ = [], [], []
            
            spanQ = 10. # +/- spanQ * Q0 is range #TODO Optional

            for ri,ti,yi,zi in zip(np.array(d_laser_container)/2.,t_laser_container,y,z):
        
                if (yi > confinement * fEL and yi < (2.-confinement) * fEL) and (zi >= (1.-spanQ)*fQ0/chocholatfactor and zi <= (1.+spanQ) * fQ0/chocholatfactor):
                
                    xRTQ.append(ti)
                    yRTQ.append(ri)
                    zRTQ.append(zi)
            
            minx = min(xRTQ)
            maxx = max(xRTQ)
            miny = min(yRTQ)
            maxy = max(yRTQ)
            
            xi = np.linspace(minx,maxx,200)
            yi = np.linspace(miny,maxy,40)    
            
            zi = griddata((xRTQ, yRTQ), np.array(zRTQ), (xi[None,:], yi[:,None]), method='linear')
            
            p7 = ax.contour(xi, yi, zi, linewidths=0.5,colors='k')
            p8 = ax.contourf(xi, yi, zi, cmap='viridis') #,norm = LogNorm())
            
            #ax.clabel(p7, inline=1, fontsize=10)
            
            ax.plot(xRT, yRT, 'ko', ms=5)
            
            # Check possible:
            #ax.plot(xRTQ, yRTQ, 'ko', ms=3)
            
            factor = [0.5,1.,2.]
            color= ['cyan','orange','red']
            for i in range(len(factor)):
                now = vgtlrl*factor[i]
                yl = [vg*t/now for t in xRTQ]
                ax.plot(xRTQ, yl, '-', color=color[i],lw=2, label=str(round(now,2)))

            ax.set_xlabel(r'$\tau_L$ / [ps]')
            ax.set_ylabel(r'$r_L$ / [um]')

            fig.colorbar(p8, shrink=1.0, aspect=5, label=r'$Q_0^C$ / [nC]')
            
            ax.axvline(x = ftL, linestyle='--', color='k')
            ax.axhline(y = frL, linestyle='--', color='k')
            
            plt.title(r'$E_L $ = '+str(fEL)+r'J')
            
            leg = ax.legend(loc='center right', borderaxespad=0., frameon=True, facecolor='white', framealpha = 0.8,
               ncol=1, prop={'size': 12})
               
            leg.set_title(r'$v_g \tau_L / r_L$', prop = {'size':12})
            
            tickstep = (max(plotrange_tL)-min(plotrange_tL))/4.
            ax.set_xticks([min(plotrange_tL) + w*tickstep for w in range(5)])
            
            ax.set_ylim(plotrange_rL)
            ax.set_xlim(plotrange_tL)
            
            plt.tight_layout()
            plt.savefig('comparative_2D_RTQwith'+str(fEL)+'J_case'+str(workmetal)+'.png')

            if showplots == 1 : plt.show()
            plt.clf()    
    #EXIT
    
elif pilotorplotter == 2 :

        # fetch logfile
        with open(cwd+'/log.txt',"r") as ofile:
            content = ofile.readlines()
        content = [x.strip(' \r\n\t').split() for x in content]
        # Container for Data
        metal_container = []
        N_iks_container = []
        dt_container = []
        Nb_Th_container = []
        t_laser_container = []
        E_laser_container = []
        C_abs_container = []
        w_laser_container = []
        d_laser_container = []
        d_targe_container = []
        e_targe_container = []
        Q_container = []
        # Parsa Data that is comparable materialwise
        for line in content:
            if workmetal == float(line[0]):
                metal_container.append(float(line[0]))
                N_iks_container.append(float(line[1]))
                dt_container.append(float(line[2]))
                Nb_Th_container.append(float(line[3]))
                t_laser_container.append(float(line[4]))
                E_laser_container.append(float(line[5]) * float(line[6]))
                C_abs_container.append(float(line[6]))
                w_laser_container.append(float(line[7]))
                d_laser_container.append(float(line[8]))
                d_targe_container.append(float(line[9]))
                e_targe_container.append(float(line[10]))
                Q_container.append(float(line[11]))
        # Evaluate Data Energy Resolution
        r_laser = np.array(d_laser_container)/2./math.sqrt(math.log(2.)) #Gausz Spot
        t_laser = np.array(t_laser_container)*math.sqrt(math.pi)/ (2.*math.sqrt(math.log(2.))) #Gausz Pulse
        P_laser = np.array(E_laser_container)/t_laser
        I_laser = P_laser  / ( math.pi* r_laser**2)
        
        a0 = 0.853*np.array(w_laser_container)*10.*np.sqrt(I_laser)
        
        T_hot_e = []
        
        for a in a0:
            if a > 0.03 :
              T_hot_e.append(max(0.47*a**(2./3.), math.sqrt(1.+a**2)-1.))
            else :
              T_hot_e.append(3.*a**(4./3.))

        E_res = np.array(Nb_Th_container) * np.array(T_hot_e) / np.array(N_iks_container)
        
        #TODO kick out all above a given minresolution
        
        # Run Tests of Convergence simulations - TODO
        #Test 1 Q1 with Nb_Th, N_iks, dt  (reference case)
        #Test 2 : Nb_Th, N_iks, 2*dt  (testing temporal dynamic)
        #Test 3 : Nb_Th, 2*N_iks, dt  (testing energy resolution)
        #Test 4 : 2*Nb_Th, 2*N_iks, dt (testing maximal energy value)
        # +> TEST OK IF ALL CHARGES AGREE
        
        # Plot Major Dependencies
        x = (t_laser * math.pi* r_laser**2)
        y = np.array(E_laser_container)
        z = np.array(Q_container)
        
        xRT,yRT,zRT = [], [], []
        
        for ri,ti,yi,zi in zip(np.array(d_laser_container)/2.,t_laser_container,y,z):
        
            if (yi > confinement * fEL and yi < (2.-confinement) * fEL):
            
                xRT.append(ti)
                yRT.append(ri)

        # Use the obtained knowledge to get a more generalized graph with multiple charges..
        # .. needs a lot of data
        
        fig = plt.figure()
        ax = plt.axes()
        
        xRTQ,yRTQ,zRTQ,TRTQ = [], [], [], []

        for ri,ti,yi,zi,th in zip(np.array(d_laser_container)/2.,t_laser_container,y,z,T_hot_e):
    
            if (yi > confinement * fEL and yi < (2.-confinement) * fEL) :
            
                if ri >= min(plotrange_rL) and ri <= max(plotrange_rL):
                
                     if ti >= min(plotrange_tL) and ti <= max(plotrange_tL):
                        
                        xRTQ.append(ti)
                        yRTQ.append(ri)
                        zRTQ.append(zi)
                        TRTQ.append(th)
        
        minx = min(xRTQ)
        maxx = max(xRTQ)
        miny = min(yRTQ)
        maxy = max(yRTQ)
        
        xi = np.linspace(minx,maxx,200)
        yi = np.linspace(miny,maxy,40)    
        
        # Plot discharge plot first
        zi = griddata((xRTQ, yRTQ), np.array(zRTQ), (xi[None,:], yi[:,None]), method='linear')
        
        p7 = ax.contour(xi, yi, zi, linewidths=0.5,colors='k')
        
        cbar_min = np.min(zRTQ)
        cbar_max = np.max(zRTQ)
        levelsc = 35
        cbarlabels = np.linspace(np.floor(cbar_min), np.ceil(cbar_max), num=levelsc, endpoint=True)
        
        p8 = ax.contourf(xi, yi, zi, levelsc, vmin=cbar_min, vmax=cbar_max, cmap='viridis') #,norm = LogNorm())
        
        #ax.clabel(p7, inline=1, fontsize=10)
        
        if showevalpoints:
            ax.plot(xRT, yRT, 'ko', ms=5)
        
        # Check possible:
        #ax.plot(xRTQ, yRTQ, 'ko', ms=3)
        
        plotratios = plotratios
        plottext = 'ratios'
        color= ['cyan','orange','red','magenta','gold','orangered','lime']
        
        if isinstance(plotratios, list):
            if plotratios[0] != None:
                for now in range(len(plotratios)):
                    yl = [vg*t/plotratios[now] for t in xRTQ]
                    ax.plot(xRTQ, yl, '-', color=color[now],lw=2, label=str(round(plotratios[now],2)))
                    plottext = plottext+'_'+str(round(plotratios[now],2))
                    
                    leg = ax.legend(loc='center right', borderaxespad=0., 
                                    frameon=True, facecolor='white', framealpha = 0.8,
                                    ncol=1, prop={'size': 12})
           
                    leg.set_title(r'$v_g \tau_L / r_L$', prop = {'size':12})

        ax.set_xlabel(r'$\tau_L$ / [ps]')
        ax.set_ylabel(r'$r_L$ / [um]')

        cobar = fig.colorbar(p8, shrink=1.0, aspect=5, label=r'$Q_0^C$ / [nC]')
        
        #cobar.set_ticks(cbarlabels)
        #cobar.set_ticklabels(cbarlabels)
        
        ax.axvline(x = ftL, linestyle='--', color='k')
        ax.axhline(y = frL, linestyle='--', color='k')
        
        plt.title(r'$E_L $ = '+str(fEL)+r'J')
        
        tickstep = (max(plotrange_tL)-min(plotrange_tL))/3.
        ax.set_xticks([min(plotrange_tL) + w*tickstep for w in range(4)])
        
        ax.set_ylim(plotrange_rL)
        ax.set_xlim(plotrange_tL)
        
        plt.tight_layout()
        savefile =('comparative_2D_RTQwith'+str(fEL)+'J_case'+str(workmetal)+'_'+plottext).replace('.','p')
        plt.savefig(savefile+'.png')

        if showplots == 1 : plt.show()
        plt.clf() 
        
        # Plot T_hot plot second
        
        fig = plt.figure()
        ax = plt.axes()
        
        ti = griddata((xRTQ, yRTQ), np.array(TRTQ), (xi[None,:], yi[:,None]), method='linear')
        
        p9 = ax.contour(xi, yi, ti, linewidths=0.5,colors='k')
        
        cbar_min = np.min(TRTQ)
        cbar_max = np.max(TRTQ)
        levelsc = 35
        cbarlabels = np.linspace(np.floor(cbar_min), np.ceil(cbar_max), num=levelsc, endpoint=True)
        
        p10 = ax.contourf(xi, yi, ti, levelsc, vmin=cbar_min, vmax=cbar_max, cmap='viridis') #,norm = LogNorm())
        
        if showevalpoints:
            ax.plot(xRT, yRT, 'ko', ms=5)
        
        plotratios = plotratios
        plottext = 'ratios'
        color= ['cyan','orange','red','magenta','gold','orangered','lime']
        if isinstance(plotratios, list):
            if plotratios[0] != None:
                for now in range(len(plotratios)):
                    yl = [vg*t/plotratios[now] for t in xRTQ]
                    ax.plot(xRTQ, yl, '-', color=color[now],lw=2, label=str(round(plotratios[now],2)))
                    plottext = plottext+'_'+str(round(plotratios[now],2))
                    
                    leg = ax.legend(loc='center right', borderaxespad=0., 
                                    frameon=True, facecolor='white', framealpha = 0.8,
                                    ncol=1, prop={'size': 12})
           
                    leg.set_title(r'$v_g \tau_L / r_L$', prop = {'size':12})

        ax.set_xlabel(r'$\tau_L$ / [ps]')
        ax.set_ylabel(r'$r_L$ / [um]')

        cobar = fig.colorbar(p10, shrink=1.0, aspect=5, label=r'$T_h^C$ / [MeV]')
        
        #cobar.set_ticks(cbarlabels)
        #cobar.set_ticklabels(cbarlabels)
        
        ax.axvline(x = ftL, linestyle='--', color='k')
        ax.axhline(y = frL, linestyle='--', color='k')
        
        plt.title(r'$E_L $ = '+str(fEL)+r'J')
        
        tickstep = (max(plotrange_tL)-min(plotrange_tL))/3.
        ax.set_xticks([min(plotrange_tL) + w*tickstep for w in range(4)])
        
        ax.set_ylim(plotrange_rL)
        ax.set_xlim(plotrange_tL)
        
        plt.tight_layout()
        savefile =('comparative_2D_RTTwith'+str(fEL)+'J_case'+str(workmetal)+'_'+plottext).replace('.','p')
        plt.savefig(savefile+'.png')

        if showplots == 1 : plt.show()
        plt.clf()

print(" EXITCUTE")
               
